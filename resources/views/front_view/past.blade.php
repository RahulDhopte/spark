@extends('front_view.layout')

@section('first content')
<section class="upcoming-event-banner">
  <div class="container">
    <div class="row">
        <div class="banner-logo">
            <div class="col-12 col-md-5">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="{{url('/upcoming/events')}}" class="spark_logo"><img src="{{URL::asset('/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
    </div>
        <div class="col-12 col-md-7">
        <ul class="event-list">
          <li><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{url('/upcoming/events')}}">Upcoming Events</a></li>
          <li class=<?php echo (Request::path() == "past/events") ?'active':'' ?>><a href="{{url('/past/events')}}">Past Events</a></li> 
          <li><a href="{{url('/gallery')}}">Gallery</a></li>
        </ul>
    </div>
        </div>
        </div>
      
      <div class="row">
          <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-6 vote-now">
              <h1 class="Vote2">Past Events</h1>
              <span>keep an eye for exciting events </span>
          </div>
      </div>
  </div><!-- container end -->
</section><!-- home banner section end -->
@endsection

@section('third content')
<section class="participants-list">
  <div class="container">
    <div class="row">
<!--
      <div class="col-12 col-md-12">
        <h2>Page Title</h2>
          <p>Sub Line</p>
      </div>
-->
    </div>
      <!-- Grid row -->
<div class="row">
  
    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
    		@foreach($years_data as $year) 
           <div class="text-center col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h3 style="text-decoration: underline;">{{ $year }}</h3><br/> 
          </div>
            @foreach($event as $row)
              @if(date("M,Y",strtotime($row->start_time))== $year)
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 df">
                <div class="upcoming-event-card">
                <img src="{{Config::get('app.bliss_url').'/events/sparkicon/medium_img/'.$row->spark_icon}}" alt="">
                <h3>{{$row->title}}</h3>
                <p>{{strlen($row->description) > 250? substr($row->description, 0, 250).'...' : $row->description}}</p>
                <span><a href="{{url('/event/details/'.encrypt_data($row->id).'/past')}}">VIEW DETAILS</a></span>
                </div>
            </div>
            @endif  
            @endforeach
            @endforeach
            <!--  <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 df">
                <div class="upcoming-event-card">
                <img src="images/upcoming-events/2.jpg" alt="">
                <h3>Navaratri Events</h3>
                <p>Here comes festival of lights music and dance coupled with essence of togetherness! NSWC (NeoSOFT Staff Welfare Committee) brings you Navras Navrat</p>
                <span><a href="up-coming-event-details.html">VIEW DETAILS</a></span>
                </div>
             </div>
            
             <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 df">
                <div class="upcoming-event-card">
                <img src="images/upcoming-events/3.jpg" alt="">
                <h3>Diwali Celebration</h3>
                <p>The Festival of lights gives us a reason to smile and believe that good finally conquers evil! Let us together enjoy this feeling, NSWC brings you a</p>
                <span><a href="up-coming-event-details.html">VIEW DETAILS</a></span>
                </div>
             </div>
            
            
             <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 df">
                <div class="upcoming-event-card">
                <img src="images/upcoming-events/4.jpg" alt="">
                <h3>Christmas</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's. </p>
                <span><a href="up-coming-event-details.html">VIEW DETAILS</a></span>
                </div>
             </div>
            
            
             <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 df">
                 <div class="upcoming-event-card">
                        <img src="images/upcoming-events/5.jpg" alt="">
                        <h3>New Year Celebration</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum. </p>
                        <span><a href="up-coming-event-details.html">VIEW DETAILS</a></span>
                        </div>
              </div> -->
            
            
             
            
            
                </div>
            

            
    </div>
</div>
</div>
  <!--</div>-->
</section> 
@endsection