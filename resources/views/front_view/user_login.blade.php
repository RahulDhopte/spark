@extends('layouts.app')

@section('content')
<div class="container"> 
        <div class="row">
             <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
            <div class="loginLogo">
              <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/img/images/neosoft.svg')}}" alt="neosoft-logo"></a>
              <a href="JavaScript:Void(0);" class="bliss_logo"><img src="{{URL::asset('/images/bliss-logo.png')}}" alt="Bliss logo"></a>
            </div>
            </div>
          
       </div>
          
        <div class="row">
            
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
              
            <div class="login-coverBg">
                
              <div class="login-panel">
                <h4>Sign In</h4>
                <div class="home-login">
                  <form method="POST" action="{{ route('login_front') }}">
                    {{ csrf_field() }}
                    <div class="login-block">
                      <div class="form-group login-emailId">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                      </div>
                      <div class="form-group login-pass">
                        <input type="password" class="form-control" name="password" id="pwd" placeholder="Password">
                      </div>
                    </div><!-- login block part end -->
                    <div class="checkbox mt-2 d-inline-block">
                      <input type="checkbox"><label> Remember me</label>
                      <input type="hidden" name="admin_status" value="0">
                    </div>
                    <p>Use MIS Credentials to Login</p>
                    <button type="submit" class="btn btn-submit mt-4 btn-bliss ml-3">Login</button>
                  </form><!-- form end -->
                
                </div> <!-- home login end -->
              </div><!-- login panel end -->
                
            </div><!-- login block end -->
              
          </div><!-- left panel end -->
            
           <div class="d-none d-sm-block  col-12 col-sm-12 col-md-6 col-lg-6 col-xl-7">
               
               <div class="LogingWc-Text">
                   
                   
                   <h1>Celebrating</h1>
                   <p>Happiness</p>
               
               
               </div>
               
          </div>
            
        </div>
          
      </div>

@endsection
