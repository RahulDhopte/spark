@extends('front_view.layout')

@section('first content')
<section class="vote-banner">
  <div class="container">
    <div class="row">
        <div class="banner-logo">
            <div class="col-12 col-md-5">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="{{url('/upcoming/events')}}" class="spark_logo"><img src="{{URL::asset('/img/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
    </div>
        <div class="col-12 col-md-7">
        <ul class="event-list">
          <li><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{url('/upcoming/events')}}">Upcoming Events</a></li>
          <li><a href="{{url('/past/events')}}">Past Events</a></li> 
          <li class=<?php echo (Request::path() == "gallery") ?'active':'' ?>><a href="{{url('/gallery')}}">Gallery</a></li> 
        </ul>
    </div>
        </div>
        </div>
      
      <div class="row">
          <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-6 vote-now">
              <h1>Gallery</h1>
<!-- <span>53 out of 70 participants have uploaded thier work for voting.</span>-->
          </div>
      </div>
  </div><!-- container end -->
</section><!-- home banner section end -->
 @endsection

@section('third content')    
<section class="participants-list"  id="list">
  <div class="container">
      <!-- Grid row -->
<div class="row">
  <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div align="center">    
             <div class="container">
                        <div  class="owl-carousel" id="YearCount">
                            
                        
                        <div class="item"><button class="btn btn-default filter-button"><a href="#" class="active">Dadar</a></button></div>
    <div class="item"><button class="btn btn-default filter-button"><a href="#">Rabale</a></button></div>
    <div class="item"><button class="btn btn-default filter-button"><a href="#">Prabhadevi</a></button></div>
    <div class="item"><button class="btn btn-default filter-button"><a href="#">Pune</a></button></div>
    <div class="item"><button class="btn btn-default filter-button"><a href="#">Ahmedabad</a></button></div>
   <div class="item"><button class="btn btn-default filter-button">Bangaluru </button></div>
                            
                        
                        
                       </div>
                 </div>
        </div>
      </div>
    
</div>
  </div>
      
      <div class="container-fluid">
       <div id="Blissphoto"><!--Blissphoto Start-->
           
           <div class="row gallery-title"><!--row Start-->
             <div class="col-12">
                 <h2>Dadar</h2>
               </div>
           </div>
                              
                            <div class="row no-gutters"><!--row Start-->


           
                               <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/1.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/1.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>

                               <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/2.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/2.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>


                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/3.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/3.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/4.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/4.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/5.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/5.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/6.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/6.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>


                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/7.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/7.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/8.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/8.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>


                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/9.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/9.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/10.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/10.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/3.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/3.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-3 col-6 thumb">
                                    <a href="{{URL::asset('/img/images/gallery/4.jpg')}}"  class="fancybox " rel="ligthbox">
                                        <img src="{{URL::asset('/img/images/gallery/4.jpg')}}" class="zoom img-fluid"  alt="">
                                    </a>
                                </div>


                             </div><!--row Start-->
                              
                              
                           </div><!--Blissphoto End-->
          
          <div class="row">
          <div class="col-12 load-btn">
              <button class="btn btn-default">Load more</button>
              </div>
          </div>
</div>
</section>  
@endsection