@extends('front_view.layout')

@section('first content')
<section class="home-banner1">
  <div class="container">
    <div class="row">
        <div class="banner-logo">
            <div class="col-12 col-md-5">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="{{url('/upcoming/events')}}" class="spark_logo"><img src="{{URL::asset('/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
    </div>
        <div class="col-12 col-md-7">
        <ul class="event-list">
        <li class=<?php echo (Request::path() == "upcoming/events") ?'active':'' ?>><a href="{{url('/upcoming/events')}}">Upcoming Events</a></li>
            <li class=<?php echo (Request::path() == "past/events") ?'active':'' ?>><a href="{{url('/past/events')}}">Past Events</a></li> 
            <li class=<?php echo (Request::path() == "gallery") ?'active':'' ?>><a href="{{url('/gallery')}}">Gallery</a></li>
        </ul>
    </div>
        </div>
        </div>
      
      <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-6 vote-now45">
                 <h1 class="Vote2">Past Events</h1>
              <span>keep an eye for exciting events </span>
          </div>
          <div class="col-12 col-sm-10 col-md-8 col-lg-5 col-xl-5 offest-xl-1">
              <div class="event-name">
                  <span>{{$day}}</span>
                  <h1>{{$event[0]->title }}</h1>
              </div>
          </div>
      </div>
  </div><!-- container end -->
</section><!-- home banner section end -->
@endsection

@section('second content')
<section>
  <div class="container">
    <div class="row des-box">
      <div class="col-12 col-md-6 description">
        <h2>Description</h2>
          <p>{{$event[0]->description}}</p>
          <ul>
            <li><strong>Date:</strong> {{$day}}</li>
            <li><strong>Team Size:</strong> Min {{$event[0]->min }} Max {{$event[0]->max }}</li>
            <li><strong>Time:</strong> {{$start_time }} - {{$end_time }} IST</li>
        </ul>
      </div>
        <div class="col-12 col-md-6 venues">
            <h2>Venues</h2>
        <ul>
          @foreach($locations as $location)
            <li>{{ $location->location_name }}</li>
          @endforeach
        </ul>
            <img src="{{URL::asset('/images/venue-bg.jpg')}}" />
      </div>
    </div>
  </div>
  <div style="text-align: center;" >
  {{-- {{ dd(isset($team_id[0])) }} --}}
  @if(isset($team_id[0]) && $today > $endreg)
  <a href="{{ url('/submission/'.$team_id[0]->id.'/'.$event[0]->id) }}" class="member" >Submission</a>
  @endif
</div>
</section><!-- About Green Flat Design section end -->
@endsection