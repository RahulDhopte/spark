@extends('front_view.layout')

@section('first content')
<section class="home-banner1">
  {{-- {{ dd($event[0]->max == 0) }} --}}
  <div class="container">
    <div class="row">
        <div class="banner-logo">
            <div class="col-12 col-md-5">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="{{url('/upcoming/events')}}" class="spark_logo"><img src="{{URL::asset('/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
    </div>
        <div class="col-12 col-md-7">
        <ul class="event-list">
          <li><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{url('/upcoming/events')}}">Upcoming Events</a></li>
          <li><a href="{{url('/past/events')}}">Past Events</a></li> 
          <li><a href="{{url('/gallery')}}">Gallery</a></li>
        </ul>
    </div>
        </div>
        </div>
      
      <div class="row">
        @if($year == 'coming')
          <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-6 vote-now45">
                 <h1 class="Vote2">Upcoming Events</h1>
              <span>keep an eye for exciting events </span>
          </div>
        @else
          <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-6 vote-now">
              <h1 class="Vote2">Past Events</h1>
              <span>keep an eye for exciting events </span>
          </div>
        @endif

          <div class="col-12 col-sm-10 col-md-8 col-lg-5 col-xl-5 offest-xl-1">
              <div class="event-name">
                  <span> {{$day}}</span>
                  <h1>{{$event[0]->title }}</h1>
              </div>
          </div>
      </div>
  </div><!-- container end -->
</section><!-- home banner section end -->
@endsection

@section('second content')
<section>
  <div class="container">
    <div class="row des-box">
      <div class="col-12 col-md-6 description">
        <h2>Description</h2>
          <p>{{strlen($event[0]->description) > 300? substr($event[0]->description, 0, 300).'...' : $event[0]->description}}</p>
          <ul>
            <li><strong>Date:</strong> {{$day}}</li>
            <li><strong>Team Size:</strong> Min - {{$event[0]->min }} {{ $event[0]->max == '0' ?'' :'Max - ' .$event[0]->max }}</li>
        <li><strong>Time:</strong> {{$start_time }} To {{$end_time }}</li> 
        </ul>
        @if($today <= $endreg && !in_array(session('user_id'), $member_id))
        <span ><button class="btn btn-default sub"><a href="{{url('/registration/'.encrypt_data($event[0]->id))}}">REGISTRATION</a></button></span>
        @endif
      </div>
        <div class="col-12 col-md-6 venues">
            <h2>Venues</h2>
        <ul>
          @foreach($locations as $location)
            <li>{{ $location->location_name }}</li>
          @endforeach
        </ul>
            <img src="{{URL::asset('/images/venue-bg.jpg')}}" />
      </div>
    </div>
  </div>
  @if(isset($teams[0]))
  <div class="container" >
      <!-- Grid row -->
<div class="row">
  <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div align="center">    
             <div class="container">
                        <div  class="owl-carousel" id="YearCount">
                        <button class="filter-button" data-filter="all">All</button>   
                        @foreach($locations as $location)
                        <div class="item"><button class="btn btn-default filter-button" data-filter="{{ $location->location_name }}{{ $location->id }}">{{ $location->location_name }}</button></div>
                        @endforeach
                       </div>
            </div>
          </div>  
   </div>
</div>          
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

          <div class="row">
            @foreach($locations as $location)
                @foreach($teams as $team)
                @if($team->location_id == $location->id)
                  <div id="" style="display: flex;" class="gallery_product col-lg-3 col-md-6 col-sm-6 col-xs-12 filter {{ $location->location_name }}{{ $location->id }}">
                    <div class="three-pic-card">
                     <ul>
                        @php
                          $count=count($team->teamMembers);
                        @endphp
                        @if($count == 1)
                            <li><img src="{{URL::asset('/images/card/dadar/no-pic.jpg')}}" /></li>
                            <li><img src="{{URL::asset('/images/card/dadar/1.jpg')}}" data-toggle="tooltip" data-placement="top"/></li>
                            <li><img src="{{URL::asset('/images/card/dadar/no-pic.jpg')}}" /></li>
                        @else 
                          @foreach($team->teamMembers as $Member)
                              <li><img src="{{URL::asset('/images/card/dadar/1.jpg')}}" data-toggle="tooltip" data-placement="top"/></li>
                          @endforeach   
                        @if($count<3)
                          @php
                            $extra = 3 - $count;
                          @endphp
                          @for($i = $extra;$i>0;$i--)
                              <li><img src="{{URL::asset('/images/card/dadar/no-pic.jpg')}}" /></li>
                          @endfor 
                        @endif
                        @endif
                    </ul>
                    <h3 style="cursor: pointer;" class="member" data-id = {{ $team->id }}>{{ $team->team_name }}</h3>
                    <span>{{ count($team->teamMembers) }} Member</span>
                  </div>
                </div>
                @endif
                @endforeach
            @endforeach
          </div>
        </div>
</div>
@endif
<div>
  <form id="del-team" method="POST" action="{{ route('del-team') }}">
    {{ csrf_field() }}
    <input id="team_id" type="hidden" name="team_id" value="">
  </form>
</div>
<div style="text-align: center;" >
  {{-- {{ dd(isset($team_id[0])) }} --}}
  @if($today > $startvote && $endvote >= $today )
  <button class="btn btn-default sub"><a href="{{ url('/voting/'.encrypt_data($event[0]->id)) }}" >Voting</a></button>
  @endif
</div>

</section><!-- About Green Flat Design section end -->
@endsection

@section('extra js')
 <script type="text/javascript">
    $(document).on('click', '.member', function(e) {
        var $this = $(this);
        var id = $this.data('id');
        $.ajax({
            type: "get",
            url: "{{'/get/member'}}",
            data: {
             "id": id,

           },
           cache: false,
           success: function (response) {
             $('.name').remove();
             $('.teamName').remove();
             var i = 0;
             console.log(response.id);
             $('.heading').append('<h3 style="text-align:center;" class="teamName"><strong style="">Team - '+response.team_name.team_name+'</strong></h3>');
             $.each(response.name, function( index, value ) {
              // alert( index + ": " + value );
              // console.log(response.team_name.team_name);
              i++;
              
             $('.content').append('<p class="name"><strong style="">Member</strong> '+i+': '+value.name+'</p>');
            });
             $('#team_id').val(response.id);
             if (response.check) {
                  $('#del_team').prop('disabled',false);
             }
             else{
                  $('#del_team').prop('disabled',true);
             }
             var id = $(this).attr("data-id");
             $('#myModal').modal('show');
           }
         });
        // console.log(id);
    });
    $(document).on('click', '#del_team', function(e) {
    $('.modal1').text('Are you sure , you want to delete the team ?');
    $('#myModal').modal('hide');
    $('#myModal2').modal('show');
  });

  $(document).on('click', '#yes', function(e) {
    $("#del-team").submit();
  }); 
 </script>
    <script>
        $('#YearCount').owlCarousel({
            responsive:{
                0:{
                    items:{{ $loc_count + 1 }}
                },
                576:{
                    items:{{ $loc_count + 1 }}
                },
                768:{
                    items:{{ $loc_count + 1 }}
                },
                992:{
                    items:{{ $loc_count + 1 }}
                }
                ,
                1200:{
                    items:{{ $loc_count + 1 }}
                }
            }
        });
    </script> 
@endsection
