@extends('front_view.layout')

@section('first content')
<section class="vote-banner">
  <div class="container">
    <div class="row">
        <div class="banner-logo">
            <div class="col-12 col-md-5">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="{{url('/upcoming/events')}}" class="spark_logo"><img src="{{URL::asset('/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
    </div>
        <div class="col-12 col-md-7">
        <ul class="event-list">
          <li><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{url('/upcoming/events')}}">Upcoming Events</a></li>
          <li><a href="{{url('/past/events')}}">Past Events</a></li> 
          <li><a href="{{url('/gallery')}}">Gallery</a></li>
        </ul>
    </div>
        </div>
        </div>
      
      <div class="row">
          <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-6 vote-now">
              <h1 class="Vote1">Voting Now</h1>
              {{-- <span>53 out of 70 participants have uploaded thier work for voting.</span> --}}
          </div>
      </div>
  </div><!-- container end -->
</section><!-- home banner section end -->
 @endsection

@section('third content')    
<section class="voting-details"><!--voting-details Start-->
    
  <div class="container"><!--container Start-->
      
    <div class="row">
      <div class="col-12 col-md-10">
        <h2>{{ $event->title }}</h2>
      </div>
        <div class="col-12 col-md-2">
        <span><a href="{{ url('voting/'.encrypt_data($submission[0]->event_id)) }}">Back</a></span>
      </div>
    </div>
      
    <div class="row">
      <div class="col-12 col-md-12">
          <div class="row">
              
          <div class="col-12 col-md-6 vote-details">
            <h3>{{ $submission[0]->Title }}</h3>
              <p>{{ $submission[0]->Description }}</p>
              {{-- <span>- By {{ $names[$created_id]['name'] }} ({{ $names[$created_id]['tech'] }}) On {{ $day }}</span> --}}
              
              <h4>Team</h4>
              <ul>
                @foreach($names as $name)
                 <li>{{ $name['name'] }} ({{ $name['tech'] }})</li>
                @endforeach 
              </ul>
          </div>
              
          <div class="col-12 col-md-6">
                        <div  class="owl-carousel" id="EventList">
                            
                        @foreach($submission[0]->subImages as $img)
                        <div class="item"><img src="{{ asset('events/submission/'.$submission[0]->event_id.'/'.$submission[0]->team_id.'/medium_img/'.$img->image) }}"></div>
                        @endforeach
                       </div>
          </div>
              
          </div>
      </div>
        <div class="col-12 col-md-12">
        <div class="row">
        <form id="rating_validate" style="width: 100%;" method="POST" action="{{ route('rating') }} ">
          {{ csrf_field() }}
          <div class="col-12 col-md-12 vote-details mt-0">
            <h4>Rating Now <!--<span>(40)</span>--></h4>
                <div class="star-rating">
                      <input type="radio" id="5-stars" name="rate" value="5" />
                      <label for="5-stars" class="star">&#9733;</label>
                      <input type="radio" id="4-stars" name="rate" value="4" />
                      <label for="4-stars" class="star">&#9733;</label>
                      <input type="radio" id="3-stars" name="rate" value="3" />
                      <label for="3-stars" class="star">&#9733;</label>
                      <input type="radio" id="2-stars" name="rate" value="2" />
                      <label for="2-stars" class="star">&#9733;</label>
                      <input type="radio" id="1-star" name="rate" value="1" />
                      <label for="1-star" class="star">&#9733;</label>
                 </div>
        
          </div>
           {{-- <div class="col-12 col-md-12 vote-details mt-0">
               
               <h4>Rating Now <!--<span>(40)</span>--></h4>
               
               <div class="rating">
                   
                  <div class="rate">
                <!--
                    <input type="radio" id="star10 someclass" name="rate" value="10"/>
                    <label for="star10" title="">10 stars</label>$submission[0]->Title
                -->

                    <input type="radio" id="star5" name="rate" value="5" />
                    <label for="star5" title="">5 stars</label>
                    <input type="radio" id="star4" name="rate" value="4" />
                    <label for="star4" title="">4 stars</label>
                    <input type="radio" id="star3" name="rate" value="3" />
                    <label for="star3" title="">3 stars</label>
                    <input type="radio" id="star2" name="rate" value="2" />
                    <label for="star2" title="">2 stars</label>
                    <input type="radio" id="star1" name="rate" value="1" />
                    <label for="star1" title="">1 star</label>
                  </div>
                   
                </div>
               
           </div> --}}
        
           <div class="col-12 col-md-12 vote-details mt-0">
                <h4>Comments <span>({{ count($votings) }})</span></h4>
                   <input type="hidden" name="team_id" value="{{ $submission[0]->team_id}}">
                   <input type="hidden" name="event_id" value="{{ $submission[0]->event_id }}">
                   <input type="hidden" name="sub_id" value="{{ $submission[0]->id }}">
                   <textarea name="comment" placeholder="Type comment"></textarea>
                   @if(isset($members))
                   @if(!array_key_exists(session('user_id'), $members))
                   <button disabled class="btn">Add Comment</button>
                   @endif
                   @else
                   <button disabled class="btn">Add Comment</button>
                   @endif
               
           </div>
           </form>
           </div>
         </div>
           <div class="col-12 col-md-12">
               <ul class="comments">
                @if(isset($votings))
                @foreach($votings as $voting)
                   <li>
                       <div class="row"><!--row Start-->
                        
                           <div class="col-xl-10 col-lg-10 col-md-10 col-6">
                               <span>{{ $members[$voting->user_id]['name'] }}</span>
                               <span>{{ $voting->created_at->toDateString() }}</span>
                           </div>
                           @php
                               $review_array = [];
                               $avg = $voting->rating;
                               $review_array[1] = "one"; 
                               $review_array[2] = "two"; 
                               $review_array[3] = "three"; 
                               $review_array[4] = "four";
                               $review_array[5] = "five"; 
                               $rating =$review_array[$avg];
                           @endphp
                           <div class="col-xl-2 col-xl-2 col-md-2 col-6">
                                <!--<div class="rating-one"></div>-->
                                <div class="rating-{{ $rating }}"></div>
                                <!--<div class="rating-three"></div>-->
                                <!--<div class="rating-four"></div>-->
                                <!--<div class="rating-five"></div>-->
                           </div>
                       
                      </div><!--row End-->
                       <!--<span>Sandeep Patil</span>-->
                      <p>{{ $voting->comment }}</p>
                       
                    </li>
                @endforeach  
                @endif
                {{-- <li>
                   <div class="row"><!--row Start-->
                    
                       <div class="col-xl-10 col-lg-10 col-md-10 col-6">
                           <span>Sonali Chavan</span>
                           <span>06.11.2019</span>
                       </div>
                       
                       <div class="col-xl-2 col-xl-2 col-md-2 col-6">
                            <!--<div class="rating-one"></div>-->
                            <!--<div class="rating-two"></div>-->
                            <!--<div class="rating-three"></div>-->
                            <div class="rating-four"></div>
                            <!--<div class="rating-five"></div>-->
                       </div>
                   
                  </div><!--row End-->
                   <!--<span>Sandeep Patil</span>-->
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                    
                </li> --}}
                   
                   
                   
               </ul>
           </div>
    </div>
           
           
  </div><!--container End-->
    
</section><!--voting-details End-->
@endsection
@section('extra js')
<script type="text/javascript">
  $(document).on('click', '.star', function(e) {
    $('.btn').prop('disabled', false);
  });
</script>
@endsection