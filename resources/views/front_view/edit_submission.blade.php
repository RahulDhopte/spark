@extends('front_view.layout')

@section('first content')
	<section class="home-banner">
  <div class="container">
    <div class="row">
        <div class="banner-logo">
            <div class="col-12 col-md-5">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="{{url('/upcoming/events')}}" class="spark_logo"><img src="{{URL::asset('/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
    </div>
        <div class="col-12 col-md-7">
        <ul class="event-list">
          <li><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{url('/upcoming/events')}}">Upcoming Events</a></li>
          <li><a href="{{url('/past/events')}}">Past Events</a></li> 
          <li><a href="{{url('/gallery')}}">Gallery</a></li>
        </ul>
    </div>
        </div>
        </div>
      
      <div class="row">
          <div class="col-12 col-md-5 col-lg-7 col-xl-7 home-banner-image">
                 <img src="{{URL::asset('/images/home-bg.png')}}" alt="banner-image" />
          </div>
          <div class="col-12 col-md-7 col-lg-5 col-xl-5">
              <div class="event-name">
                  <span>{{$day}}</span>
                  <h1>{{$event_data[0]->title }}</h1>
              </div>
          </div>
      </div>
  </div><!-- container end -->
</section><!-- home banner section end -->
@endsection

@section('second content')
	<section>
  <div class="container">
    <div class="row">
        <div class="col-12 col-md-10">
        </div>
        <div class="col-12 col-md-2">
        <span><a style="font-size: 18px;text-align: right;border: 1px solid #00c3ff;border-radius: 30px;
    padding: 12px 50px;" href="{{ url('/event/details/'.encrypt_data($event_data[0]->id).'/past') }}">Back</a></span>
      </div>
    </div>
    <div class="row des-box">
      <div class="col-12 col-md-12 registration">
        <h2>Submission</h2>
               <form id="sub_img" method="POST" action="{{ route('edit-submission') }}" enctype="multipart/form-data">  
               	{{ csrf_field() }}
                <input type="hidden" name="id" value="{{$submission_data[0]->id }}">
                <input type="hidden" name="event_id" value="{{$submission_data[0]->event_id }}">
                <input type="hidden" name="team_id" value="{{$submission_data[0]->team_id}}">
<!--                   <textarea placeholder="Type comment"></textarea>-->
                <h3>{{ $submission_data[0]->Title }}</h3>
                <p>{{ $submission_data[0]->Description }}</p>
                <span>- By {{ $name[$submission_data[0]->created_by]['name'] }} ({{ $name[$submission_data[0]->created_by]['tech'] }}) </span>
                   <label>Images</label>
                   <div id= "">
                   <div class=""><button style="max-height: 45px;" class="btn">Submit</button>
                   <span><input style="width: 35%;" name="Image[]" id="uploadFile"  type="file" placeholder="Choose File" class="mandatory_fildes form-control" multiple></span>
                   
                   </div>
                   </div>
                   <div class="row"> 
                    @if(isset($submission_data[0]->subImages))
                    {{-- <div> --}}
                     @foreach($submission_data[0]->subImages as $img)
                     <div class="col-4 submission" >
                     <img style="width: 90%;" src="{{ asset('events/submission/'.$submission_data[0]->event_id.'/'.$submission_data[0]->team_id.'/'.$img->image) }}">
                     @if(count($submission_data[0]->subImages) > 1)
                     <i class="fa fa-times-circle del" data-id="{{ $img->id }}" ></i></div>
                     @endif
                     @endforeach

                    @endif 
                   </div>
                   
               </form>
      </div>
    </div>


  </div>
  <form id="del_form" method="POST" action="{{route('delete-image')}}">
    {{ csrf_field() }}
      <input type="hidden" name="id" id="img-id" value="">
  </form>
</section><!-- About Green Flat Design section end -->
@endsection

@section('extra js')
<script type="text/javascript">
$(document).on('click', '.del', function(e) {
    // $(this).closest(".imgclass").remove();
    // $(this).remove();
    $('.modal1').text('Are you sure , you want to delete the image ?');
    var id = $(this).data('id');
    // console.log(id);
    $('#img-id').val(id);
    $('#myModal2').modal('show');
  });
  $(document).on('click', '#no', function(e) {
    $('#myModal2').modal('hide');
  });

  $(document).on('click', '#yes', function(e) {
    $("#del_form").submit();
  });                      
</script>
@endsection