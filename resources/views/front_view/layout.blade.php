<!DOCTYPE html>
<html lang="en">
<head>
  <title>Spark - Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <link href="{{asset('css/animate.css')}}" rel="stylesheet">
  <link href="{{asset('css/jquery.fancybox.min.css')}}" rel="stylesheet">
  {{-- <link rel="shortcut icon" type="image/png" href="images/favicon.png"/> --}}
  <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
  <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
  <link href="{{asset('css/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/jquery-ui.theme.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/spark-main.css')}}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/component.css') }}" />
  <style type="text/css">
  .error{
    color: red !important;
    margin: 0px 0px 0px 0px !important;
  }
  .ui-menu{
    width: 337px;
    max-height: 350px;
    display: block;
    overflow-y: scroll;
    border-radius:10px;
  }
  .ui-menu-item{
    padding:2px;
  }
  .filter-button{
    font-size: 18px;
    border: 1px solid #00c3ff;
    border-radius: 30px;
    text-align: center;
    color: #00c3ff;
    margin-bottom: 30px;
    background: transparent;
    min-width: 160px;
    padding: 12px 0;
  }
  .modal-body {
    position: relative;
    padding: 20px;
}
.modal-content {
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
    margin: auto;
    display: block;
    width: 100%;
    max-width: 700px;
    }

 .sub{
    font-size: 18px;
    border: 1px solid #00c3ff;
    border-radius: 30px;
    text-align: center;
    color: #00c3ff;
    margin-bottom: 30px;
    background: transparent;
    min-width: 160px;
    padding: 12px 0;
 }   
 .filter-button .active{
  text-align: center;
    color: #ffffff;
    background-color: #6e2fe2;
}
 }
</style>
</head>
<body>

  <div class="toolbar">

    <div class="container">

      <div class="row">

        <div class="col-xl-4 col-lg-4 col-md-5 col-6 toolbar-left">
          <span class="User-1"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></span>

          <span class="User-2">{{session('user_name')}}</span>
        </div>

        <div class="col-xl-8 col-lg-8 col-md-7 col-6 ">

          <ul class="headerRight text-right">

           {{-- <li><a href="{{ url('registration') }}" class="btn btn-primary submit45">Registration</a></li> --}}
           <!--<li class="headerUser1"><a href="#"><img src="images/notification.png"></a></li>-->

           <li class="headerUser">
            <div class="dropdown">
             {{--  <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <img src="{{URL::asset('/images/more-bt.svg')}}">
              </a> --}}
              {{-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                <a class="dropdown-item" href="#">Dropdown Item</a>
                <a class="dropdown-item" href="#">Dropdown Item</a>

              </div> --}}
            </div>
          </li>


          <li class="dropdown notification_dropdown">

            {{-- <a href="javascript:void(0)" class="prodCart dropdown-toggle"  href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span>2</span><img src="{{URL::asset('/images/bellIcon.png')}}">
            </a> --}}

            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

              <h3>Notifications (<span>3</span>)</h3>

              <div class="dropdown-item">
                <div class="img-wrap"><img src="{{URL::asset('/images/img-sm.png')}}"></div>
                <h4>Rakesh Shah <span>congratulated you</span></h4>
                <a href="#" class="congrts-text">View</a>
              </div>

              <div class="dropdown-item">
                <div class="img-wrap"><img src="{{URL::asset('/images/img-sm.png')}}"></div>
                <h4>Rakesh Shah <span>congratulated you</span></h4>
                <a href="#" class="congrts-text">View</a>
              </div>

            </div>

          </li>


          <li class="headerUser">
            <!-- Menu Toggle Button -->
            @if (Route::has('login'))
            <div style="margin: 0px 5px;">
             

             
            <a href="{{ route('user_logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();"><img src="{{URL::asset('/images/logout-icon.png')}}"></a>

            <form id="logout-form" action="{{ route('user_logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
            @else
            <a style="color:white;"  href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a style="color:white;"  href="{{ route('register') }}">Register</a>
            @endif

            
          </div>
          @endif
         
        </li>

      </ul>

    </div>

  </div>

</div>

</div><!-- toolbar section end -->


@yield('first content')

<section>
  @yield('second content')
</section><!-- About Green Flat Design section end -->



<!-- Grid row -->
@yield('third content')

<footer><!--footer Start-->

  <div class="container"><!-- footer container Start-->

    <div class="row">
      <div class="col-12 footer-title">EMPLOYEE <strong>FIRST</strong> EMPLOYEE <strong>ALWAYS</strong></div>
      <div class="col-12 footer-subtitle" align="center">An Ecosystem that Empowers and Fuels the Spirit of our Employees</div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neosoft-spark.svg')}}" alt="neosoft-spark" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neo-champions-league.svg')}}" alt="neo-champions-league" class="champion" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neosoft-collab.svg')}}" alt="neosoft-collab" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neosoft-bliss.svg')}}" alt="neosoft-bliss" /></a></div>

      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neo-wisdombytes.svg')}}" alt="neo-wisdombytes" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neo-wellness.svg')}}" alt="neo-wellness" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neo-smile.svg')}}" alt="neosmile" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neosoft-connect.svg')}}" alt="neosoft-connect" /></a></div>

      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neosoft-academy.svg')}}" alt="neosoft-academy" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neosoft-bookcafe.svg')}}" alt="neosoft-bookcafe" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neosoft-university.svg')}}" alt="neosoft-university" /></a></div>
      <div class="col-4 col-sm-3 col-md-3 col-lg-2 clo-xl-2 footer-logos"><a href="javascript:void(0)"><img src="{{URL::asset('/images/footer-logo/neosoft-excelencia.svg')}}" alt="neosoft-excelencia" /></a></div>


    </div>

  </div><!-- footer container end -->

  <div class="container-fluid copyright"><!-- footer container Strat -->

    <div class="container">
      <div class="row ">
        <div class="col-lg-6 col-sm-6 col-12" align="left"> Copyright NeoSOFT Technologies</div>
        <div class="col-lg-6 col-sm-6 col-12" align="right">Spark (A NeoSOFT Initiative)</div>
      </div>
    </div>

  </div><!-- footer container end -->

</footer><!--footer End--> 

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-body">
          {{-- <p class="modal1"></p> --}}
          <div style="border-bottom: 1px solid #e5e5e5;" class="heading">
            
          </div>
          <div style="padding: 10px;" class="content"></div>
          <div class="text-center">  
          <button style="" data-dismiss="modal" class="btn btn-primary">Close</button>
          @if(isset($team_id[0]))
          <button id='del_team' class="btn btn-danger">Delete</button>
          @endif
          </div>
        </div>
      </div>

    </div>
  </div>

  <div id="myModal2" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-body">
          <p class="modal1"></p>
          <button id='yes' class="btn btn-primary">Yes</button>
          <button id="no" data-dismiss="modal" class="btn btn-primary">No</button>
          
        </div>
      </div>

    </div>
  </div>

<script src="{{ asset('js/jquery.js')}}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
<script src="{{ asset('js/popper.min.js')}}"></script>
<script src="{{ asset('js/front_js/custom.js')}}"></script>	
<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('js/popper.min.js')}}"></script>
<script src="{{ asset('js/jquery.fancybox.min.js')}}"></script>
<script src="{{ asset('js/wow.min.js')}}"></script>
<script src="{{ asset('js/front_js/registration.js')}}"></script>
<script src="{{ asset('js/front_js/submission.js')}}"></script>
<script src="{{ asset('js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('js/gallery/modernizr.custom.js')}}"></script>
    <script src="{{ asset('js/gallery/masonry.pkgd.min.js')}}"></script>
    <script src="{{ asset('js/gallery/imagesloaded.js')}}"></script>
    <script src="{{ asset('js/gallery/classie.js')}}"></script>
    <script src="{{ asset('js/gallery/AnimOnScroll.js')}}"></script>
    <script>
      new AnimOnScroll( document.getElementById( 'grid' ), {
        minDuration : 0.4,
        maxDuration : 0.7,
        viewportFactor : 0.2
      } );
    </script>
<script>
  $(document).ready(function(){
    $(".fancybox").fancybox({
      openEffect: "none",
      closeEffect: "none"
    });

    $(".zoom").hover(function(){

      $(this).addClass('transition');
    }, function(){

      $(this).removeClass('transition');
    });
  });

</script>

  @yield('extra js')
<script>
 new WOW().init();
</script>
</body>
</html>