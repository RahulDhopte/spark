@extends('front_view.layout')

@section('first content')
	<section class="home-banner">
  <div class="container">
    <div class="row">
        <div class="banner-logo">
            <div class="col-12 col-md-5">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="{{url('/upcoming/events')}}" class="spark_logo"><img src="{{URL::asset('/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
    </div>
        <div class="col-12 col-md-7">
        <ul class="event-list">
          <li><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{url('/upcoming/events')}}">Upcoming Events</a></li>
          <li><a href="{{url('/past/events')}}">Past Events</a></li> 
          <li><a href="{{url('/gallery')}}">Gallery</a></li>
        </ul>
    </div>
        </div>
        </div>
      
      <div class="row">
          <div class="col-12 col-md-5 col-lg-7 col-xl-7 home-banner-image">
                 <img src="{{URL::asset('/images/home-bg.png')}}" alt="banner-image" />
          </div>
          <div class="col-12 col-md-7 col-lg-5 col-xl-5">
              <div class="event-name">
                  <span>{{$day}}</span>
                  <h1>{{$event[0]->title }}</h1>
              </div>
          </div>
      </div>
  </div><!-- container end -->
</section><!-- home banner section end  -->
@endsection

@section('second content')
	<section>
  <div class="container">
    <div class="row">
        <div class="col-12 col-md-10">
        </div>
        <div class="col-12 col-md-2">
        <span><a style="font-size: 18px;text-align: right;border: 1px solid #00c3ff;border-radius: 30px;
    padding: 12px 50px;" href="{{ url('/event/details/'.encrypt_data($event[0]->id).'/coming') }}">Back</a></span>
      </div>
    </div>
    <div class="row des-box">
      <div class="col-12 col-md-6 registration">
        <h2>Registration</h2>
               <form id="registration_validate" action="{{ url('/team/registration') }}"> 
               @if(Session::has('InvalidMember'))
                                    <div class="alert alert-danger">
                                        {{Session::get('InvalidMember')}}
                                    </div>
              @endif 
                <input type="hidden" name="event_id" value="{{$event[0]->id }}">
<!--                   <textarea placeholder="Type comment"></textarea>-->
                   <label>Name</label>
                    <input type="text" style="text-transform: capitalize;" name="groupname" placeholder="Name / Group Name">
                   <label>Location</label>
                   <select class="form-control" name="location">
                      @foreach($location as $loc)
                        <option value="{{ $loc['id'] }}">{{ $loc['location_name'] }}</option>
                      @endforeach
                </select>
                   <label>Participant</label>
                   <div id= "parti-div">
                      <div class="parti-name">
                        <span><input type="text" id="participant_name" class="parti-input" name="participant_name[0]" value="{{ $email[session('user_id')]['email'] }}" autocomplete="off"></span>
                     </div>
                   @for($i = 0;$i<$event[0]->min - 1;$i++)
                     <div class="parti-name">
                        <span><input type="text" id="participant_name" class="parti-input" name="participant_name[{{ $i + 1 }}]" value="" autocomplete="off"></span>
                     </div>
                   @endfor
                   </div>
                    <div class="test1" style="max-height: 100px;overflow-y: scroll;"></div>
                   {{-- <div class="parti-name">
                   <span><input type="text" name="participant-name" placeholder="Full Name & Profile Photo"></span>
                    
                   </div> --}}
                   @if(isset($event[0]->max))
                   @if($event[0]->min < $event[0]->max)
                   <div class="add-another"><a href="#">+ Add Participant</a></div>
                   @endif
                   @endif
                   <button class="btn">REGISTER</button>
               </form>
      </div>
        <div class="col-12 col-md-6 regi-pic">
           <img src="{{URL::asset('/images/event-pic.jpg')}}" />
            
      </div>
    </div>
  </div>
</section><!-- About Green Flat Design section end -->
@endsection

@section('extra js')
<script type="text/javascript">
  $(".add-another").click(function(e){
    e.preventDefault();
    var count = $('.parti-input').length;
    var input = $('.parti-input').length;
    $("#parti-div").append('<div class="parti-name"><span class="parti_add"><i class="fa fa-times-circle del" ></i><input  type="text" class="parti-input" name="participant_name['+input+']" placeholder="Full Name & Profile Photo" autocomplete="off"></span></div>');
      var max = {{ $event[0]->max }};
      input+=1;
      if ($('.parti-input').length >= max) {
        $('.add-another').hide();

      }
      $("input[name*=participant_name").each(function(){

        $(this).rules("add",{
          required:true,
          noSpace: true,
          messages:{
          }
        });
        });

      // $("#registration_validate").validate();
      
  });
  $(document).on('click', '.del', function(e) {
    console.log('dasdas');
    console.log( $(this).closest(".parti-name"));
    $(this).closest(".parti-name").remove();
    $(this).remove();
    var max = {{ $event[0]->max }};
    if ($('.parti-input').length >= max) {
        $('.add-another').hide();
      }
    else{
      $('.add-another').show();
    }  
  });
  $(document).on('keypress',".parti-input",function(){
                     var search=$(this).val();
                     console.log(search.length);
                    if(search.length>=2){
                      $.ajax({
                      type: "GET",
                      url: "{{route('check-email')}}",
                      dataType: 'json',
                      data: {
                        'search':search
                      },
                      }).done(function (result) {
                        // var namesList = Array.prototype.slice.call(result, 0);
                        // console.log(typeof namesList); $('#' + resp.id.replace('|', '\\|')).addClass('has-success animate bounce'); $('.NewClass').removeClass(".btnSaveConfirmation")
                        $(".parti-input").autocomplete({
                            source:result ,
                            // appendTo: ".test1"
                        });
                      });
                 
                    }
                      
                     
                     });
  $(document).ready(function(){
    $(".parti-input").blur(function()
    {
        var email = $(".parti-input").val();
        console.log(email);
        $.get('/check_email/'+email, function(response)
        {
            if(response.success)
            {
                 $('#email_error1').text('');
            }
            else
            {
                 // $('#email_error').remove();
                 $('#email_error1').text('Email Already Exist');
            }
        })
    });
});
                      
</script>
@endsection