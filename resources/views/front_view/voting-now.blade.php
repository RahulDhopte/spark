@extends('front_view.layout')

@section('first content')
<section class="vote-banner">
  <div class="container">
    <div class="row">
        <div class="banner-logo">
            <div class="col-12 col-md-5">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="{{url('/upcoming/events')}}" class="spark_logo"><img src="{{URL::asset('/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
    </div>
        <div class="col-12 col-md-7">
        <ul class="event-list">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{url('/upcoming/events')}}">Upcoming Events</a></li>
            <li><a href="{{url('/past/events')}}">Past Events</a></li> 
            <li><a href="{{url('/gallery')}}">Gallery</a></li>
        </ul>
    </div>
        </div>
        </div>
      
      <div class="row">
          <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-6 vote-now">
              <h1 class="Vote1">Voting Now</h1>
              
              <span>{{ count($submissions) }} out of {{ count($reg) }} participants have uploaded thier work for voting.</span>
          </div>
      </div>
  </div><!-- container end -->
</section>
 @endsection

@section('third content')    
<section class="participants-list"  id="list">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-10">
        
      </div>
        <div class="col-12 col-md-2">
        <span><a style="font-size: 18px;text-align: right;border: 1px solid #00c3ff;border-radius: 30px;padding: 12px 50px;" href="{{ url('/event/details/'.encrypt_data($event->id).'/past') }}">Back</a></span>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-12">
        <h2>{{ $event->title }}</h2>
          <p>All Participants</p>
      </div>
    </div>
      <!-- Grid row -->
<div class="row">
  <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div align="center">    
             <div class="container">
                        <div  class="owl-carousel" id="YearCount">
                        {{-- <button class="filter-button" data-filter="all">All</button> --}}
                        <div class="item"><button class="btn btn-default filter-button" data-filter="all">All </button></div>   
                        @foreach($locations as $location)
            {{-- <div class="item"><button class="btn btn-default filter-button" data-filter="ahm">Ahmedabad (0)</button></div>             --}}
                        <div class="item"><button class="btn btn-default filter-button" data-filter="{{ $location->location_name }}{{ $location->id }}">{{ $location->location_name }}</button></div>
                        @endforeach
                       </div>
                 </div>
        </div>
      </div>
    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="box-effect">
        <div class="row">
    
            @foreach($locations as $location)
            @foreach($submissions as $submission)
            @if($submission->location == $location->id)
            <div class="col-12 col-sm-6 col-md-4 filter dadar {{ $location->location_name }}{{ $location->id }}">
                    <div class="box14">
                        <img src="{{ asset('events/submission/'.$submission->event_id.'/'.$submission->team_id.'/'.$submission->subImages[0]->image) }}" alt="">
                        <div class="box-content">
                            <span class="post"><a href="{{ route('voting-details', [encrypt_data($submission->id)]) }}">VOTE NOW</a></span>
                        </div>
                        <h3>{{ $submission->Title }}</h3>
                        <span>{{ $count }} Members</span>
                        <ul>
                        <li>{{ count($submission->rating) }} Vote{{ count($submission->rating) > '1' ? '(s)':''}}  </li>
                        <li>{{ count($submission->rating) }} Comment{{ count($submission->rating) > '1' ? '(s)':''}}</li>
                        </ul>
                    </div>
                </div>
            @endif    
            @endforeach
            @endforeach
            
             {{-- <div class="col-12 col-sm-6 col-md-4 filter rabale">
                    <div class="box14">
                        <img src="images/participants-list/2.jpg" alt="">
                        <div class="box-content">
                            <span class="post"><a href="voting-details.html">VOTE NOW</a></span>
                        </div>
                        <h3>Group Title</h3>
                        <span>2 Member(s)</span>
                        <ul>
                        <li>950 Vote(s)  </li>
                        <li>878 Comment(s)</li>
                        </ul>
                    </div>
                </div>
            
            
             <div class="col-12 col-sm-6 col-md-4 filter dadar">
                    <div class="box14">
                        <img src="images/participants-list/3.jpg" alt="">
                        <div class="box-content">
                            <span class="post"><a href="voting-details.html">VOTE NOW</a></span>
                        </div>
                        <h3>Group Title</h3>
                        <span>3 Member(s)</span>
                        <ul>
                        <li>950 Vote(s)  </li>
                        <li>878 Comment(s)</li>
                        </ul>
                    </div>
                </div>
            
            
             <div class="col-12 col-sm-6 col-md-4 filter pune">
                    <div class="box14">
                        <img src="images/participants-list/4.jpg" alt="">
                        <div class="box-content">
                            <span class="post"><a href="voting-details.html">VOTE NOW</a></span>
                        </div>
                        <h3>Group Title</h3>
                        <span>2 Member(s)</span>
                        <ul>
                        <li>950 Vote(s)  </li>
                        <li>878 Comment(s)</li>
                        </ul>
                    </div>
                </div>
            
            
             <div class="col-12 col-sm-6 col-md-4 filter rabale">
                    <div class="box14">
                        <img src="images/participants-list/1.jpg" alt="">
                        <div class="box-content">
                            <span class="post"><a href="voting-details.html">VOTE NOW</a></span>
                        </div>
                        <h3>Group Title</h3>
                        <span>3 Member(s)</span>
                        <ul>
                        <li>950 Vote(s)  </li>
                        <li>878 Comment(s)</li>
                        </ul>
                    </div>
                </div>
            
            
             <div class="col-12 col-sm-6 col-md-4 filter pune">
                    <div class="box14">
                        <img src="images/participants-list/2.jpg" alt="">
                        <div class="box-content">
                            <span class="post"><a href="voting-details.html">VOTE NOW</a></span>
                        </div>
                        <h3>Group Title</h3>
                        <span>2 Member(s)</span>
                        <ul>
                        <li>950 Vote(s)  </li>
                        <li>878 Comment(s)</li>
                        </ul>
                    </div>
                </div> --}}

            
    </div>
</div>
</div>
  </div>
</section> 
@endsection
@section('extra js')
    <script>
        $('#YearCount').owlCarousel({
            responsive:{
                0:{
                    items:{{ $loc_count + 1 }}
                },
                576:{
                    items:{{ $loc_count + 1 }}
                },
                768:{
                    items:{{ $loc_count + 1 }}
                },
                992:{
                    items:{{ $loc_count + 1 }}
                }
                ,
                1200:{
                    items:{{ $loc_count + 1 }}
                }
            }
        });
    </script>
@endsection