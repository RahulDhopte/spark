@extends('layouts.login_layout')

@section('content')
<section class="login-section">
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
        <div class="loginLogo">
          <a href="https://www.neosofttech.com/" target="_blank" class="neosoft-logo"><img src="{{URL::asset('/img/images/neosoft.svg')}}" alt="neosoft-logo"></a>
          <a href="JavaScript:Void(0);" class="spark_logo"><img src="{{URL::asset('/img/images/spark-logo.svg')}}" alt="spark logo"></a>
        </div>
        </div>
        </div>
      
       <div class="row">
      <div class="col-12 col-md-6 col-lg-6 col-xl-5">
        <div class="login-coverBg">
          <div class="login-panel">
             
            <h4>Sign In</h4>
            <div class="home-login">
              <form method="POST" action="{{ route('login_front') }}">
                {{ csrf_field() }}
                @if(Session::has('invalid_user'))
                                    <div class="alert alert-danger">
                                        {{Session::get('invalid_user')}}
                                    </div>
              @endif
                <div class="login-block">
                  <div class="form-group login-emailId">
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                  </div>
                  <div class="form-group login-pass">
                    <input type="password" name="password" class="form-control" id="pwd" placeholder="Password">
                  </div>
                </div><!-- login block part end -->
                <p>Use MIS Credentials to Login</p>
                <button type="submit" class="btn submit mt-4" onclick="location.href='index.html'">Login</button>
              </form><!-- form end -->
            </div> <!-- home login end -->
          </div><!-- login panel end -->
        </div><!-- login block end -->
      </div><!-- left panel end -->
           
        <div class="col-12 col-md-4 col-lg-4 col-xl-4 offset-lg-1 offset-xl-2 login-banner-image">
                 <!--<img src="images/home-bg.png" alt="banner-image" />-->
          </div>   
      
    </div>
  </div>
</section>
@endsection
