@extends('admin.layout')

@section('content')
 <!-- 
 -->
 <h3>User Master</h3>
 <div class="box">

 	<h3 style="margin-left: 10px;">All Users</h3>
 	<br>
 	<div align="right">
 		<a href="{{url('add/user/view')}}" style=" margin: 0px;margin-top: -30px;margin-right: -20px;" class="btn btn-primary">Add</a>
 		<a id="mult_del" href="" style=" margin: 0px 25px;margin-top: -30px;" class="btn btn-danger">Delete</a>
 	</div>

 	<div class="container">

 		<table id="user_table" class="table table-bordered" style="width:100%">
 			<thead>
 				<tr>
 					<th><input style="margin-left: -8px;" type = "checkbox" id="select_all" name="mult_delete[]"></th>
 					<th>Sr No.</th>
 					<th>Email</th>
 					<th>Role</th>
 					<th>Action</th>
 				</tr>
 			</thead>
 		</table>
 	</div>
 </div>
 <form id="del_form" action="/delete/user" method="POST">
 	{{ csrf_field() }}
 	<input id="hidden_del" type="hidden" name="hidden_del" value="">
 </form>
 <script>

 	$(document).ready(function() {
 		$('#user_table').DataTable({
 			"processing": true,
 			"serverSide": true,
 			"bFilter": false,
 			"bPaginate": false,
 			"bInfo" : false,
 			 "ordering": false,
 			"ajax": "{{ url('/user_data') }}",
 			"columns":[
 			{ "data": "check" },
 			{ "data": 'DT_RowIndex', name: 'DT_RowIndex',"sClass": "datatables_action"  },
 			{ "data": "email" },
 			{ "data": "role" },
 			{data: 'action', name: 'action', orderable: false, searchable: false},
 			]
 		});
 	});
 	$(document).on('click', '#select_all', function(e) {
 		if($(this).is(':checked'))
 		{
 			$('.delete').prop('checked', true);
 		}
 		else
 		{
 			$('.delete').prop('checked', false);
 		}
 	});
 	
 	function multiple_Delete(e) {

 		var $this = $(this);
 		var id = new Array();
 		$("input[type='checkbox']").each(function() {
 			if($(this).prop('checked'))
 			{
 				if($(this).val() != "on")
 				{
 					id.push($(this).val());
 				}
 			}
 		});
 		console.log(id);
 		$.ajax({
 			type: "POST",
 			url: "{{'/delete/multiple/users'}}",
 			dataType: 'html',
 			data: {
 				"_token": "{{ csrf_token() }}",
 				'id':id},
 				cache: false,	
 				success: function(responce){
 					location.reload();
 				}
 			})
 	}
 		$(document).on('click', '#single_del', function(e) {
 			var name = $(this).attr("data-name");
 			$('.modal1').text('Are you sure , you want to delete '+name+' ?');
 			var id = $(this).attr("data-id");
 			$('#hidden_del').val(id);
			$('#myModal').modal('show');
 		});

 		$(document).on('click', '#no', function(e) {
 			$('#myModal').modal('hide');
 		});

 		$(document).on('click', '#yes', function(e) {
 			$("#del_form").submit();
 		});

 		$(document).on('click', '#mult_del', function(e) {
 			e.preventDefault();
 			$('.modal2').text('Are You Sure , you want to delete all the seleted record?');
			$('#myModal1').modal('show');
 		});

 		$(document).on('click', '#confirm', function(e) {
 			multiple_Delete(e);
 		});
 		
 		$(document).on('click', '#cancel', function(e) {
 			$('#myModal1').modal('hide');
 		});

 </script>
 @endsection