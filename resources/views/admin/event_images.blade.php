@extends('admin.layout')

@section('content')
<h3>Event Images</h3>
<a style="float: right;margin-top: -37px;margin-right: 30px;" class="btn btn-primary" href="{{url('event/list')}}">Back</a>
<div class="box">
	<div class="row justify-content-center" style="margin-left: 0px;">
			<div class="box-header with-border">
				<h3 class="box-title" style="font-size: 20px;">Add images for {{$event['title']}}</h3>
			</div>
			<br>
			<form id="image_form" action="{{ url('image-upload')}}" method="post" enctype="multipart/form-data">

				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-2">
					<label style="margin-left: 10px;font-size: 17px;" for="Image" class="">{{ __('Image') }}<!-- <span style="color:red;">*</span> --></label>
					</div>
					<div class="col-md-4">
						<input name="Image[]" id="uploadFile" required type="file" placeholder="Choose File" class="mandatory_fildes form-control" multiple>
						<input type="hidden" name="event_id" value="{{$event['id']}}">
					</div>
					<div class="col-md-3">
						<button style="" type="submit" class="btn btn-primary">
							{{ __('ADD') }}
						</button>

					</div>
				</div>
				<!-- <div  class="form-group row mb-0"> -->
				<!-- </div> -->
			</form>
			<div class="appr-unappr">
				<div class="unapproved-section">
					<div class="box-header with-border">
						<h3 class="box-title" style="font-size: 20px;">Un Approved Images</h3>
					</div>
					<div class="row UnApproved" style="margin-left: 0;margin-top: 10px;margin-right: 20px;">
						@foreach($images as $row)
						@if($row['is_approved'] == 0)
						<div class="col-md-3">
							<img class="image" src='{{URL::asset("$path"."/".$row["event_image"])}}' style="position: relative;width: 150px;">
							<input type="checkbox" id="UnApproved" value="{{$row['id']}}" style="position: absolute;top: 0;margin-left: 5px;margin-left: 5px;">
							<i class="fa fa-times-circle" id="del_img" data-id="{{$row['id']}}" data-name="{{$row['event_image']}}" style="font-size:24px;color:red;position: absolute;top: 20px;margin-left: 5px;"></i>
						</div>
						@endif
						@endforeach
					</div>
				</div>

				<div class="approved-section" style="margin-bottom: 20px;">
					<div class="box-header with-border">
						<h3 class="box-title" style="font-size: 20px;">Approved Images</h3>
					</div>
					<div class="row Approved"  style="margin-left: 0;margin-top: 10px;margin-right: 20px;" >
						@foreach($images as $row)
						@if($row['is_approved'] == 1)
							<div class="col-md-3">
								<img class="image" src='{{URL::asset("$path"."/".$row["event_image"])}}' style="position: relative;width: 150px;">
								<input type="checkbox" id="Approved" checked value="{{$row['id']}}" style="position: absolute;top: 0;margin-left: 5px;">
							</div>
						@endif
						@endforeach
					</div>
				</div>
			</div>
			<div id="myModal" class="modal">
  				<span class="close">&times;</span>
  				<img class="modal-content" id="img01">
  				<div id="caption"></div>
			</div>
			<form id="del_form" action="/delete/event/image" method="POST">
 				{{ csrf_field() }}
 				<input id="hidden_del" type="hidden" name="hidden_del" value="">
 			</form>
	</div>
</div>
</div>

<script type="text/javascript">
$(document).on('click', '#UnApproved', function(e) {
		$this = $(this);
		var img = $(this).val();
		$this.closest('div').remove();
		console.log('unApproved');
		console.log(img);
		if (img) {
			$.ajax({
				type: "POST",
				url: "{{'/is_Approved'}}",
				data: {
					"_token": "{{ csrf_token() }}",
					"id": img,

				},
				cache: false,
				success: function (response) {
					console.log(response.img_id);
					$('.Approved').append('<div class="col-md-3"><img class="image" src="'+response.url+'/'+response.name+'" style="position: relative;width: 150px;"><input type="checkbox" id="Approved" checked value="'+response.img_id+'" style="position: absolute;top: 0;margin-left: 5px;"></div>');
				}
			});
		}
	});

	$(document).on('click', '#Approved', function(e) {
		$this = $(this);
		var img = $(this).val();
		$this.closest('div').remove();
		console.log('Approved');
		console.log(img);
		if (img) {
			$.ajax({
				type: "POST",
				url: "{{'/is_UnApproved'}}",
				data: {
					"_token": "{{ csrf_token() }}",
					"id": img,

				},
				cache: false,
				success: function (response) {
					console.log(response.img_id);
					$('.UnApproved').append('<div class="col-md-3"><img class="image" src="'+response.url+'/'+response.name+'" style="position: relative;width: 150px;"><input type="checkbox" id="UnApproved" value="'+response.img_id+'" style="position: absolute;top: 0;margin-left: 5px;"><i class="fa fa-times-circle" id="del_img" data-id="'+response.img_id+'" data-name="'+response.name+'" style="font-size:24px;color:red;position: absolute;top: 20px;margin-left: 5px;"></i></div>');
				}
			});
		}
	});

	$(document).on('click', '#del_img', function(e) {
		var name = $(this).attr("data-name");
		$('.modal1').text('Are you sure , you want to delete the image ?');
		var id = $(this).attr("data-id");
		$('#hidden_del').val(id);
		$('#myModal2').modal('show');
	});
	$(document).on('click', '#no', function(e) {
		$('#myModal2').modal('hide');
	});

	$(document).on('click', '#yes', function(e) {
		$("#del_form").submit();
	});



// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementsByClassName(".image");
var modal = document.getElementById("myModal");
var modalImg = document.getElementById("img01");
$(document).on('click', '.image', function(e) {
	console.log('dasd');
  modal.style.display = "block";
  modalImg.src = this.src;
});

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
@endsection
