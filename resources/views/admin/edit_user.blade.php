@extends('admin.layout')

@section('content')
<h3>Update Users Form</h3>
<a style="float: right;margin-top: -37px;margin-right: 30px;" class="btn btn-primary" href="{{url('user/list')}}">Back</a>
  <div class="box">
  <div class="row justify-content-center">
        <div class="col-md-8">
        	<br>
            <div class="box-header with-border">
		<h3 class="box-title">Update User</h3>
    </div>
		<br>
      
    <form id="user_validate" method="POST" action="{{ url('/update/user/'.$update['id']) }}">
                       	{{csrf_field()}}	


                            <div style="border-top-left-radius: 0;border-top-right-radius: 0;border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;padding: 10px;" class="box_body"> 

                         <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $update['email'] }}" required autocomplete="email">
                            </div>
                        </div>

             

                              <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}<span style="color:red;">*</span></label>
                            <div class="col-md-6">
                            	<select name="role">
                            		@foreach($role_name as $roles)
  									<option <?php echo ($update['role_id'] == $roles['id']) ?'selected':'' ?> value="{{ $roles['id'] }}">{{ $roles['name'] }}</option>
 									@endforeach
								</select>
                            </div>
                            </div> 
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                        
                    </div>
                        
                    </form>
                </div>
            </div>
        </div>
@endsection