<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>AdminLTE 2 | Starter</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Font Awesome -->

	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">



	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
        <link rel="stylesheet" href="{{ asset('css/skin-blue.min.css	') }}">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

         <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  		<link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    	<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
    	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" /> -->
        <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>   -->

        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

        <meta name="csrf-token" content="{{ csrf_token() }}" />
       <!--  <script type="text/javascript">
          $.ajaxSetup({
          headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script> -->
<style type="text/css">
	.error{
		color: red;
	}
	
	.datatables_action{
		text-align: center;
	}

	#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.img_modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  color: #f1f1f1;
  font-size: 40px;
  margin-right: 50px;
  margin-top: 100px;
}
#img01{
	margin-top: 100px;
}
.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}

</style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<?php
	if(!isset($_SESSION)) 
	{ 
		session_start(); 
	}

	?>


	<div class="wrapper">

		<!-- Main Header -->
		<header class="main-header">

			<!-- Logo -->
			<a style="background-color: black;"  href="#" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<img src="{{URL::asset('/img/images/footer-logo/neosoft-spark.svg')}}" style="width: 70%;" alt="Bliss logo">
			</a>

			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
    <!--   <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a> -->
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
    	<ul class="nav navbar-nav">



    		<li class="dropdown user user-menu">
    			<!-- Menu Toggle Button -->
    			@if (Route::has('login'))
    			<div style="font-size: 25px;padding: 7px; margin: 0px 5px;">
    				

    				<a style="color: white; margin-left: -30px;" 
    				href="{{ route('logout') }}"
    				onclick="event.preventDefault();
    				document.getElementById('logout-form').submit();">
    				Logout
    			</a>

    			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    				{{ csrf_field() }}
    			</form>
    			@else
    			<a style="color:white;"  href="{{ route('login') }}">Login</a>

    			@if (Route::has('register'))
    			<a style="color:white;"  href="{{ route('register') }}">Register</a>
    			@endif

    			
    		</div>
    		@endif

    	</li>

    </ul>
</div>
</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel (optional) -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{URL::asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>{{session('admin_user_name')}}</p>
				<!-- Status -->
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>


		<ul class="sidebar-menu" data-widget="tree">

			<!-- Optionally, you can add icons to the links -->

			<li class="treeview  <?php echo (Request::path() == "user/list" or Request::path() == "add/user/view") ?'active open':'' ?>">
				<a href="#"><i class=""></i> <span>Users</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu ">
					<li class=<?php echo (Request::path() == "user/list") ?'active':'' ?>><a  href="{{ url('user/list') }}">Users List</a></li>
					<li class=<?php echo (Request::path() == "add/user/view") ?'active':'' ?>><a href="{{url('add/user/view')}}">Add User</a></li>
				</ul>
			</li>

			<li class="treeview  <?php echo (Request::path() == "event/list" or Request::path() == "add/event/view") ?'active open':'' ?>">
				<a href="#"><i class=""></i> <span>Events</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu ">
					<li class=<?php echo (Request::path() == "event/list") ?'active':'' ?>><a  href="{{ url('event/list') }}">Event List</a></li>
					<li class=<?php echo (Request::path() == "add/event/view") ?'active':'' ?>><a href="{{url('add/event/view')}}">Add Event</a></li>
				</ul>
			</li>


		</ul>
		<!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div>


		<!-- Main content -->
		<section style="padding: 1px;padding-left: 15px;padding-right: 15px;" class="content container-fluid">
			@if(session()->has('message.level'))
			<div class="alert alert-{{ session('message.level') }}"> 
				{!! session('message.content') !!}
			</div>
			@endif

			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
					</ul>
				</div>
			@endif
			@yield('content')

		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

	<!-- Main Footer -->
	<footer>
		<!-- To the right -->

	</footer>

	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Create the tabs -->
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
			<li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
			<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<!-- Home tab content -->
			<div class="tab-pane active" id="control-sidebar-home-tab">
				<h3 class="control-sidebar-heading">Recent Activity</h3>
				<ul class="control-sidebar-menu">
					<li>
						<a href="javascript:;">
							<i class="menu-icon fa fa-birthday-cake bg-red"></i>

							<div class="menu-info">
								<h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

								<p>Will be 23 on April 24th</p>
							</div>
						</a>
					</li>
				</ul>
				<!-- /.control-sidebar-menu -->

				<h3 class="control-sidebar-heading">Tasks Progress</h3>
				<ul class="control-sidebar-menu">
					<li>
						<a href="javascript:;">
							<h4 class="control-sidebar-subheading">
								Custom Template Design
								<span class="pull-right-container">
									<span class="label label-danger pull-right">70%</span>
								</span>
							</h4>

							<div class="progress progress-xxs">
								<div class="progress-bar progress-bar-danger" style="width: 70%"></div>
							</div>
						</a>
					</li>
				</ul>
				<!-- /.control-sidebar-menu -->

			</div>
			<!-- /.tab-pane -->
			<!-- Stats tab content -->
			<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
			<!-- /.tab-pane -->
			<!-- Settings tab content -->
			<div class="tab-pane" id="control-sidebar-settings-tab">
				<form method="post">
					<h3 class="control-sidebar-heading">General Settings</h3>

					<div class="form-group">
						<label class="control-sidebar-subheading">
							Report panel usage
							<input type="checkbox" class="pull-right" checked>
						</label>

						<p>
							Some information about this general settings option
						</p>
					</div>
					<!-- /.form-group -->
				</form>
			</div>
			<!-- /.tab-pane -->
		</div>
	</aside>
	<!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  	immediately after the control sidebar -->
  	<div class="control-sidebar-bg"></div>
  </div>
  <div id="myModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

  		<!-- Modal content-->
  		<div class="modal-content">

  			<div class="modal-body">
  				<p class="modal1"></p>
  				<button id='yes' class="btn btn-primary">Yes</button>
  				<button id="no" class="btn btn-primary">No</button>
  			</div>
  		</div>

  	</div>
  </div>

  <div id="myModal1" class="modal fade" role="dialog">
  	<div class="modal-dialog">

  		<!-- Modal content-->
  		<div class="modal-content">

  			<div class="modal-body">
  				<p class="modal2"></p>
  				<button id='confirm' class="btn btn-primary">Yes</button>
  				<button id="cancel" class="btn btn-primary">No</button>
  			</div>
  		</div>

  	</div>
  </div>
  <div id="myModal3" class="modal fade" role="dialog">
  	<div class="modal-dialog">

  		<!-- Modal content-->
  		<div class="modal-content">

  			<div class="modal-body">
  				<p class="modal4"></p>
  				<button id='change' class="btn btn-primary">Yes</button>
  				<button id="no" class="btn btn-primary">No</button>
  			</div>
  		</div>

  	</div>
  </div>

  <div id="myModal2" class="modal fade" role="dialog">
  	<div class="modal-dialog">

  		<!-- Modal content-->
  		<div class="modal-content">

  			<div class="modal-body">
  				<p class="modal1"></p>
  				<button id='yes' class="btn btn-primary">Yes</button>
  				<button id="no" class="btn btn-primary">No</button>
  				
  			</div>
  		</div>

  	</div>
  </div>

  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->


  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('js/adminlte.min.js') }}"></script>
  <script src="{{ asset('js/user.js') }}"></script>
  <script src="{{ asset('js/event.js') }}"></script>
  <script src="{{ asset('js/event_image.js') }}"></script>
<!-- <script type="text/javascript">
  
  $(".treeview-menu").click(function(e) {
   
   var $this = $(this);
    $this.addClass("active open");
   
});
</script> -->


@yield('js')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
 </body>
 </html>
