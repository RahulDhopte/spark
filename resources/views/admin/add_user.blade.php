@extends('admin.layout')

@section('content')
<h3>Users Form</h3>
<a style="float: right;margin-top: -37px;margin-right: 30px;" class="btn btn-primary" href="{{url('user/list')}}">Back</a>
  <div class="box">
  <div class="row justify-content-center">
        <div class="col-md-8">
        	<br>
            <div class="box-header with-border">
		<h3 class="box-title">Add User</h3>
    </div>
		<br>
    <form id="user_validate" method="POST" action="{{ url('add/user') }}">
                       	{{csrf_field()}}	

                            <div class="box-body">

                         <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="" required autocomplete="email">
                                <span id="email_error1" style="color: red"></span>
                                
                            </div>
                        </div>
             
                             <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>
                            <div class="col-md-6">
                            	<select name="role">
                            		
                            		@foreach($role_name as $role)
  									<option value="{{ $role['id'] }}">{{ $role['name'] }}</option>
 									@endforeach
								</select>
                            </div>
                            </div> 
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('ADD') }}
                                </button>
                               
                            </div>
                        </div>
                    </div>
                        
                        
                    </form>
                </div>
            </div>
        </div>

<script type="text/javascript">
    $(document).ready(function(){
    $("input").blur(function()
    {
        var email = $('#email').val();
        console.log(email);
        $.get('/check_email/'+email, function(response)
        {
            if(response.success)
            {
                 $('#email_error1').text('');
            }
            else
            {
                 // $('#email_error').remove();
                 $('#email_error1').text('Email Already Exist');
            }
        })
    });
});
</script>
@endsection
