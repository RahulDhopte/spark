@extends('admin.layout')

@section('content')
 <!-- 
 -->
 <h3>Event Master</h3>
 <div class="box">

 	<h3 style="margin-left: 10px;">All Events</h3>
 	<br>
 	<div align="right">
 		<a href="{{url('add/event/view')}}" style=" margin: 0px;margin-top: -30px;margin-right: -20px;" class="btn btn-primary">Add</a>
 		<a id="mult_del" href="" style=" margin: 0px 25px;margin-top: -30px;" class="btn btn-danger">Delete</a>
 	</div>

 	<div class="container" >

 		<table id="event_table" class="table table-bordered" style="width:100%;padding-right: 130px;">
 			<thead>
 				<tr>
 					<th><input style="margin-left: -8px;" type = "checkbox" id="select_all" name="mult_delete[]"></th>
 					<th>Sr No.</th>
 					<th>Title</th>
 					<th>Start Time</th>
 					<th>End Time</th>
 					<th>MIN</th>
 					<th>MAX</th>
 					<th>Bliss</th>
 					<th>Spark</th>
 					<th>Images</th>
 					<th>Action</th>
 				</tr>
 			</thead>
 		</table>
 	</div>
 </div>
 <form id="del_form" action="/delete/event" method="POST">
 	{{ csrf_field() }}
 	<input id="hidden_del" type="hidden" name="hidden_del" value="">
 </form>

 <form id="status_form" action="/status" method="POST">
 	{{ csrf_field() }}
 	<input id="hidden_id" type="hidden" name="hidden_id" value="">
 	<input id="hidden_type" type="hidden" name="hidden_type" value="">
 	<input id="hidden_value" type="hidden" name="hidden_value" value="">
 </form>
 <script>

 	$(document).ready(function() {
 		$('#event_table').DataTable({
 			"processing": true,
 			"serverSide": true,
 			"bFilter": false,
 			"bPaginate": false,
 			"bInfo" : false,
 			 "ordering": false,
 			"ajax": "{{ url('/event_data') }}",
 			"columns":[
 			{ "data": "check" },
 			{ "data": 'DT_RowIndex', name: 'DT_RowIndex',"sClass": "datatables_action" },
 			{ "data": "title" },
 			{ "data": "start_time" },
 			{ "data": "end_time" },
 			{ "data": "min" },
 			{ "data": "max" },
 			{ "data": "bliss","sClass": "datatables_action" },
 			{ "data": "spark","sClass": "datatables_action" },
 			{ "data": "image","sClass": "datatables_action" },
 			{data: 'action', name: 'action', orderable: false, searchable: false},
 			]
 		});
 	});
 	$(document).on('click', '#select_all', function(e) {
 		if($(this).is(':checked'))
 		{
 			$('.delete').prop('checked', true);
 		}
 		else
 		{
 			$('.delete').prop('checked', false);
 		}
 	});
 	
 	function multiple_Delete(e) {

 		var $this = $(this);
 		var id = new Array();
 		$("input[type='checkbox']").each(function() {
 			if($(this).prop('checked'))
 			{
 				if($(this).val() != "on")
 				{
 					id.push($(this).val());
 				}
 			}
 		});
 		console.log(id);
 		$.ajax({
 			type: "POST",
 			url: "{{'/delete/multiple/event'}}",
 			dataType: 'html',
 			data: {
 				"_token": "{{ csrf_token() }}",
 				'id':id},
 				cache: false,	
 				success: function(responce){
 					location.reload();
 				}
 			})
 	}
 		$(document).on('click', '#single_del', function(e) {
 			var name = $(this).attr("data-name");
 			$('.modal1').text('Are you sure , you want to delete '+name+' ?');
 			var id = $(this).attr("data-id");
 			$('#hidden_del').val(id);
			$('#myModal').modal('show');
 		});

 		$(document).on('click', '#no', function(e) {
 			$('#myModal').modal('hide');
 		});

 		$(document).on('click', '#yes', function(e) {
 			$("#del_form").submit();
 		});

 		$(document).on('click', '#mult_del', function(e) {
 			e.preventDefault();
 			$('.modal2').text('Are You Sure , you want to delete all the seleted record?');
			$('#myModal1').modal('show');
 		});

 		$(document).on('click', '#confirm', function(e) {
 			multiple_Delete(e);
 		});
 		
 		$(document).on('click', '#cancel', function(e) {
 			$('#myModal1').modal('hide');
 		});

 		$(document).on('click', '.status', function(e) {
		$this = $(this);
		var id = $(this).attr("data-id");
		var type = $(this).attr("data-type");
		var value = $(this).attr("data-value");
		$('#hidden_id').val(id);
		$('#hidden_type').val(type);
		$('#hidden_value').val(value);
		console.log('Approved');
		$('.modal4').text('Are you sure , you want to change status?');
        	$('#myModal3').modal('show');
		});

		$(document).on('click', '#change', function(e) {
 			$("#status_form").submit();
 		});
 		$(document).on('click', '#no', function(e) {
 			$('#myModal3').modal('hide');
 		});
 	// 	$(document).on('click', '#inactive', function(e) {
		// $this = $(this);
		// var id = $(this).attr("data-id");
		// var type = $(this).attr("data-type");
		// $this.remove();
		// console.log('Approved');
  //       	if (id) {
  //       		$.ajax({
  //               type: "POST",
  //               url: "{{'/status'}}",
  //                 data: {
  //                   "_token": "{{ csrf_token() }}",
  //                   "id": id,
  //                   "type": type,
                    
  //               },
  //               cache: false,
  //               success: function (response) {
  //               	$this = $(this);
		// 				location.reload();
  //               }
		// });
  //       	}
		// });
 </script>
 @endsection