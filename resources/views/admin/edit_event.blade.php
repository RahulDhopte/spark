@extends('admin.layout')

@section('content')
<h3>Event Master</h3>
<a style="float: right;margin-top: -37px;margin-right: 30px;" class="btn btn-primary" href="{{url('event/list')}}">Back</a>
  <div class="box">
  <div class="row justify-content-center">
        <div class="col-md-12">
            <br>
            <div class="box-header with-border">
        <h3 class="box-title">Add Event</h3>
    </div>
        <br>
    <form id="event_validate" method="POST" action="{{ url('/update/event/'.$update['id']) }}">
                        {{csrf_field()}}    

                            <div class="box-body" style="background-color: #0000000f; border: 1px solid;padding: 36px 86px 15px 90px;margin: 0 auto;">

                         <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-right">{{ __('Title') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                <input id="title" type="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $update['title'] }}" required autocomplete="title">
                                <span id="email_error1" style="color: red"></span>
                                
                            </div>
                         </div>
                         <div class="form-group row">
                            <label for="description" class="col-md-2 col-form-label text-right">{{ __('Description') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                <textarea style="width: 314px;height: 91px;" id="description" name="description"  required="required"  rows="5" >{{ $update['description'] }}</textarea>

                            </div>
                         </div>
                         <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label text-right">{{ __('Date') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                
                                <input name="date" id="datepicker" value="{{ date(config('app.date_format'), strtotime($update['date'])) }}" width="270" readonly />
                                
                            </div>
                         </div>
                          <div class="form-group row">
                            <label for="start_time" class="col-md-2 col-form-label text-right">{{ __('Start Time') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                <!-- <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="" required autocomplete="date"> -->
                                <input type="text" name="start_time" value="{{$update['start_time']}}" id="start_time" width="270" readonly />
                                
                            </div>
                         </div>
                         <div class="form-group row">
                            <label for="end_time" class="col-md-2 col-form-label text-right">{{ __('End Time') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                <!-- <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="" required autocomplete="date"> -->
                                <input type="text" name="end_time" value="{{$update['end_time']}}" id="end_time" width="270" readonly />
                                
                            </div>
                         </div>
                         <div class="form-group row">
                            <label for="min" class="col-md-2 col-form-label text-right">{{ __('Min') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                <!-- <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="" required autocomplete="date"> -->
                                <select name="min" id="min">
                                    <option <?php echo ($update['min']=='1') ?'selected':'' ?> value="1">1</option>
                                    <option <?php echo ($update['min']=='2') ?'selected':'' ?> value="2">2</option>
                                    <option <?php echo ($update['min']=='3') ?'selected':'' ?> value="3">3</option>
                                    <option <?php echo ($update['min']=='4') ?'selected':'' ?> value="4">4</option>
                                    <option <?php echo ($update['min']=='5') ?'selected':'' ?> value="5">5</option>
                                    <option <?php echo ($update['min']=='6') ?'selected':'' ?> value="6">6</option>
                                    <option <?php echo ($update['min']=='7') ?'selected':'' ?> value="7">7</option>
                                    <option <?php echo ($update['min']=='8') ?'selected':'' ?> value="8">8</option>
                                    <option <?php echo ($update['min']=='9') ?'selected':'' ?> value="9">9</option>
                                    <option <?php echo ($update['min']=='10') ?'selected':'' ?> value="10">10</option>
                                </select>
                                
                            </div>
                         </div>
                         <div class="form-group row">
                            <label for="max" class="col-md-2 col-form-label text-right">{{ __('Max') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                <!-- <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="" required autocomplete="date"> -->
                                <select name="max" id="max">
                                    <option value="0">Select</option>
                                    <option <?php echo ($update['max']=='1') ?'selected':'' ?> value="1">1</option>
                                    <option <?php echo ($update['max']=='2') ?'selected':'' ?> value="2">2</option>
                                    <option <?php echo ($update['max']=='3') ?'selected':'' ?> value="3">3</option>
                                    <option <?php echo ($update['max']=='4') ?'selected':'' ?> value="4">4</option>
                                    <option <?php echo ($update['max']=='5') ?'selected':'' ?> value="5">5</option>
                                    <option <?php echo ($update['max']=='6') ?'selected':'' ?> value="6">6</option>
                                    <option <?php echo ($update['max']=='7') ?'selected':'' ?> value="7">7</option>
                                    <option <?php echo ($update['max']=='8') ?'selected':'' ?> value="8">8</option>
                                    <option <?php echo ($update['max']=='9') ?'selected':'' ?> value="9">9</option>
                                    <option <?php echo ($update['max']=='10') ?'selected':'' ?> value="10">10</option>
                                </select>
                                
                            </div>
                         </div>
                         <div class="form-group row">
                            <label  class="col-md-2 col-form-label text-right">{{ __('Status') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                                
                               <input type="radio" name="status" value="1" <?php echo ($update['status'] == "1") ?'checked':'' ?>> Active
                            <input type="radio" name="status" value="0" <?php echo ($update['status'] == "0") ?'checked':'' ?>> Inactive
                            </div>
                         </div>
                         <div class="form-group row">
                            <label  class="col-md-2 col-form-label text-right">{{ __('Bliss') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                               <input type="radio" name="bliss" value="1" <?php echo ($update['is_bliss'] == "1") ?'checked':'' ?>> Active
                            <input type="radio" name="bliss" value="0" <?php echo ($update['is_bliss'] == "0") ?'checked':'' ?>> Inactive
                            </div>
                         </div>
                         <div class="form-group row">
                            <label  class="col-md-2 col-form-label text-right">{{ __('Spark') }}<span style="color:red;">*</span></label>

                            <div class="col-md-6">
                               <input type="radio" name="spark" value="1" <?php echo ($update['is_spark'] == "1") ?'checked':'' ?>> Active
                            <input type="radio" name="spark" value="0" <?php echo ($update['is_spark'] == "0") ?'checked':'' ?>> Inactive
                            </div>
                         </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-2 text-right">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                               
                            </div>
                        </div>
                    </div>
                        
                        
                    </form>
                </div>
            </div>
        </div>

<script type="text/javascript">
    $('#datepicker').datepicker({
            uiLibrary: 'bootstrap',
            format: 'yyyy-mm-dd'
        });

    $( "#start_time" ).timepicker({
        change: function(time) {
            // the input field
            var value = "'"+$(this).val()+"'";
            $("#end_time").timepicker('destroy');
            $("#end_time").timepicker({
                timeFormat: 'hh:mm p',
                minTime: value
            })
           
        }
    });
      $('#end_time').timepicker({
        change: function(time) {
            // the input field
            var value = "'"+$(this).val()+"'";
            
            $("#start_time").timepicker('destroy');
            $("#start_time").timepicker({
                timeFormat: 'hh:mm p',
                maxTime: value
            })
           
        }
    });

     $("#min").change(function(){
        var value = $(this).val();
        $("#max").children().remove();
        $('#max').append('<option value="0">Select</option>');
       for (var i = value; i <= 10; i++) {
        $('#max').append('<option value='+i+'>'+i+'</option>');
            }
    }); 
</script>
@endsection
