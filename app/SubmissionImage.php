<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmissionImage extends Model
{
    protected $table = 'submission_images';
    protected $fillable = [
        'submission_id','image','created_by','updated_by',
    ];
    public function submission(){
    	return $this->belongsTo('App\Submission');
    }
}
