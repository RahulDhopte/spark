<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    protected $table = 'winners';
    protected $fillable = [
        'event_id','team_id','location_id','created_by','updated_by',
    ];

    public function Team(){
        return $this->hasMany("App\Team", "id", "team_id");
    }
}
