<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $fillable = [
        'title','description','date','status','start_time','end_time','min','max','is_bliss','is_spark','created_by','updated_by',
    ];

    public function eventApprovedImage(){
    	return $this->hasMany('App\EventImage', 'event_id', 'id')->where("is_approved", "1");
    }


    public function eventTotalImage(){
    	return $this->hasMany('App\EventImage', 'event_id', 'id');
    }
}
