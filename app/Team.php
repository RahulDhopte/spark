<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
     protected $table = 'team';
    protected $fillable = [
        'team_name','event_id','location_id','created_by','updated_by',
    ];

    public function teamMembers(){
    	return $this->hasMany('App\TeamMember', 'team_id', 'id');
    }

    public function submission()
    {
        return $this->hasMany('App\Submission', 'team_id', 'id');
    }

}
