<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $table = 'submission';
    protected $fillable = [
        'Title','Description','event_id','team_id','location','created_by','updated_by',
    ];
    public function subImages(){
    	return $this->hasMany('App\SubmissionImage', 'submission_id', 'id');
    }
    public function Teams(){
    	return $this->hasMany('App\TeamMember', 'team_id', 'team_id');
    }
    public function events(){
    	return $this->belongsTo('App\Event', 'event_id', 'id');
    }

    public function rating(){
        return $this->hasMany('App\Voting', 'submission_id', 'id');
    }
}
