<?php

namespace App\Http\Middleware;

use Closure;

class checkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(session()->all());
         if (session()->has('admin_user_id') && session()->has('admin_is_valid')) {
            return redirect('/user/list');
        }
        return $next($request);
    }
}
