<?php

namespace App\Http\Middleware;

use Closure;
use App\Submission;

class Voting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = decrypt_data($request->route()->parameter('id'));
        $submissions = Submission::with('Teams','subImages','events','rating')->where('event_id',$id)->get();
        if (isset($submissions[0])) {
            # code...
            return $next($request);
        }
        else
        {
            return redirect('/event/details/'.encrypt_data($id).'/past');
        }
    }
}
