<?php

namespace App\Http\Middleware;

use Closure;
use App\TeamMember;
use App\Team;
use App\Submission;
use Redirect;

class checkTeam
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next 
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $team = decrypt_data($request->route()->parameter('team'));
        $event = decrypt_data($request->route()->parameter('event'));
        $user_id = session('user_id');
        $member = Team::whereHas('teamMembers',function($e) use ($user_id){
                    $e->where('member_id',$user_id);
                })->where('event_id',$event)->get();
        // $member = TeamMember::with('team')->where([['member_id',session('user_id')],['team_id',$team]])->get();
        // dd($member);
        $submission = Submission::where('team_id',$team)->get();
        // dd($submission);
        if (isset($member[0])) {
            // dd($member[0]->team->id == $team );
        if ($member[0]->id == $team && isset($submission[0])) {
            return redirect('/edit/submission/'.encrypt_data($submission[0]->id)); 
        }
        elseif ($member[0]->id == $team ) {


            return $next($request);
        }
        }
        return redirect::back();
        
    }
}
