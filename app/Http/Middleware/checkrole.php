<?php

namespace App\Http\Middleware;

use Closure;
use auth;
use App\User;

class checkrole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $id = session('admin_user_id');
         $getRoles = User::with("role")->where('id',$id)->get()->toArray();
         session(['admin_user_role_name'=>$getRoles[0]['role']['name']]);
          // dd(session()->all());
         if ( in_array('Super Admin', $getRoles[0]['role']) ) {
            return $next($request);
        }
        elseif (in_array('Admin', $getRoles[0]['role'])) {
            // dd('hre');
            return redirect('/event/list');
        }
        else
        {
            return redirect(route('/control/login'));
        }
        // return redirect(route('front'));
    }
}
