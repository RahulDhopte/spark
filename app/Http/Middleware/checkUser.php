<?php

namespace App\Http\Middleware;

use Closure;
use Route;

class checkUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(session()->all());
        if (session()->has('user_id') && session()->has('user_is_valid')) {
            return $next($request);
        }

         return redirect('/login');
    }
}
