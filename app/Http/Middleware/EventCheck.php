<?php

namespace App\Http\Middleware;

use Closure;
use App\Event;

class EventCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $event = Event::where([['start_time','<=', date('Y-m-d')],['end_time','>=' , date('Y-m-d')],['is_spark' ,'=', "1"]])->get();
        if (isset($event[0])) {
                return $next($request);
            }
        else{
            return redirect('/upcoming/events');
        }        
    }
}
