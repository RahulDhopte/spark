<?php

namespace App\Http\Controllers\Front_end;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Event;
use App\Location;
use App\Team;
use Config;
use App\TeamMember;
use App\EventImage;
use App\Winner;
use App\Submission;
use App\SubmissionImage;
use App\Voting;
use Image, File;
use Redirect;
use DB;
use Carbon\Carbon;

class FrontController extends Controller
{
    public function index()
    {	
        $event = Event::where([['start_time','<=', date('Y-m-d')],['end_time','>=' , date('Y-m-d')],['is_spark' ,'=', "1"]])->get();
        // dd($event);
        $id = $event[0]->id;
        $temp = Carbon::parse($event[0]->start_time);
        $day = $temp->format('d M,Y - l');
        $temp3 = Carbon::parse($event[0]->end_reg);
        $start = Carbon::parse($event[0]->start_time);
        $start_time = $start->format('l - g:i A');
        $end = Carbon::parse($event[0]->end_time);
        $end_time = $end->format('l - g:i A');
        $endreg = $temp3->format('Y-m-d');
        $today = date("Y-m-d");
        $loc = explode(',', $event[0]->location);
        $locations = Location::whereIn('id',$loc)->get();
        $loc_count = count($locations);
        $teams = Team::with('teamMembers')
        ->where('event_id',$id)->get();
        $user_id = session('user_id');
        $team_id = Team::whereHas('teamMembers',function($e) use ($user_id){
                    $e->where('member_id',$user_id);
                })->where('event_id',$event[0]->id)->get();
        return view('front_view/index',compact('event','day','start_time','end_time','locations','loc_count','teams','team_id','endreg','today'));
    }
    public function gallery()
    {   
        
        return view('front_view/gallery');
    }
    public function upcoming()
    {	
        // dd();
    	$event = Event::where([['start_time','>' , date('Y-m-d')],['is_spark' ,'=', "1"]])->get();
        if(isset($event[0])){
            foreach ($event as $key => $year) {
            $temp =Carbon::parse($year->start_time);
            $years[] = $temp->format('M,Y');
            }
            $years_data = array_unique($years);
        } 
        // dd($years_data);
        return view('front_view/upcoming',compact('event','years_data'));
    }

    public function past()
    {   

        $event = Event::where([['end_time','<' , date('Y-m-d')],['is_spark' ,'=', "1"]])->get(); 
        foreach ($event as $key => $year) {
        $temp =Carbon::parse($year->start_time);
        $years[] = $temp->format('M,Y');
        }
        $years_data = array_unique($years);
        // dd($years);
        return view('front_view/past',compact('event','years_data'));
    }
    public function event_details($id,$year)
    {
        $id = decrypt_data($id);
    	$event = Event::where('id' , $id)->get();
    	$temp = Carbon::parse($event[0]->start_time); 
        $temp6 = Carbon::parse($event[0]->end_time); 
    	$day = $temp->format('d M,Y - l');
        $temp2 = Carbon::parse($event[0]->start_reg);
        $temp3 = Carbon::parse($event[0]->end_reg);
        $temp4 =Carbon::parse($event[0]->start_vote);
        $temp5 =Carbon::parse($event[0]->end_vote);
        $reg = $temp2->format('Y-m-d');
        $endreg = $temp3->format('Y-m-d');
        $startvote = $temp4->format('Y-m-d');
        $endvote = $temp5->format('Y-m-d');
        $endtime = $temp6->format('Y-m-d');
        $today = date("Y-m-d");
        $start = Carbon::parse($event[0]->start_time);
        $start_time = $start->format('l - g:i A');
        $end = Carbon::parse($event[0]->end_time);
        $end_time = $end->format('l - g:i A');
        $loc = explode(',', $event[0]->location);
        $locations = Location::whereIn('id',$loc)->get();
        $loc_count = count($locations);
        $teams = Team::with('teamMembers')
        ->where('event_id',$id)->get();
        $member_id = [];
        foreach ($teams as $team) {
            foreach ($team->teamMembers as $key => $members) {
            $member_id[] = $members->member_id;
            }
        }

        $user_id = session('user_id');
         $team_id = Team::whereHas('teamMembers',function($e) use ($user_id){
                    $e->where('member_id',$user_id);
                })->where('event_id',$event[0]->id)->get();
    	return view('front_view/upcoming_events_details',compact('event','day','reg','today','locations','start_time','end_time','endreg','startvote','endvote','teams','team_id','member_id','year','loc_count','endtime'));
    }

    public function load_img(Request $request)
    {
        $offset = $request->post('offset');
        $id = $request->post('id');
        // dd($offset);
        // dd($id);
        $total = EventImage::where('event_id',$id)->get()->count();
        $image =Event::with(['eventApprovedImage' => function($q) use($offset){
            $q->offset($offset)->limit(5);
        }])->where('id',$id)->get();
        // dd(DB::getQueryLog());
        //  dd($image);
        $total_img = $offset + 5;
        $year = now()->year;
        $path = '/events/'.$year.'/'.$id.'/image';
        $offset = $offset + 5;
        if ($image) {
            return response()->json(['success' => true,'image' => $image,'url' => url($path),'image' => $image ,'total_img' => $total_img,'id' => $id,'offset' => $offset,'total'=>$total ]);
        }
        else{
            return response()->json(['success' => false]);
        }

    }
    public function event(Request $request)
    {

    	$id = $request->post('id');
        $offset = $request->post('offset');
        // dd($offset);
        $total = EventImage::where('event_id',$id)->get()->count();
        $image = Event::with(['eventApprovedImage' => function($q) use($offset){
            $q->offset($offset)->limit(5);
        }])->where('id',$id)->get();
        $total_img = $image[0]->eventApprovedImage->count();
        $year = now()->year;
        $path = '/events/'.$year.'/'.$id.'/image';
        $offset = '5';
        if ($image) {
          return response()->json(['success' => true,'image' => $image,'url' => url($path),'image' => $image ,'total_img' => $total_img,'id' => $id,'offset' => $offset,'total'=>$total ]);
      }
      else{
          return response()->json(['success' => false]);
      }
  }

  public function voting($id)
  {
    $id = decrypt_data($id);
    $submissions = Submission::with('Teams','subImages','rating')->where('event_id',$id)->get();
    $reg = Team::where('event_id',$id)->get();
    // dd($submissions);
    $event = Event::find($id);
    // dd($event);
    $loc= [];

    foreach ($submissions as $key => $submission) {

        $loc[] =$submission->location; 
    }
     // dd($loc);
    $locations = Location::whereIn('id',$loc)->get();
    $loc_count = count($locations);
    // dd($locations);
    if (isset($submissions[0])) {
        $count = count($submissions[0]->Teams); 
    }
    else{

    $count = 0;
    }
    // dd($count); 
    return view('front_view/voting-now',compact('locations','submissions','count','loc_count','reg','event'));
  }

  public function voting_details($id)
  {
    $id = decrypt_data($id);
    $submission = Submission::with('subImages')->where('id',$id)->get();
    // $reg = Team::where('event_id',$id)->get();
    // dd($submission);
    $teams = TeamMember::with('team')->where('team_id',$submission[0]->team_id)->get();
    // dd($teams);
    $created_id = $teams[0]->team->created_by;
    $temp = Carbon::parse($teams[0]->team->created_at); 
    $day = $temp->format('d M,Y-l');
    // dd($day);
    foreach ($teams as $key => $team) {
        $member_id[] = $team->member_id; 
    }
    $names = getUserDetails($member_id);
    // dd($teams);
    $event = Event::find($submission[0]->event_id);
    $votings = Voting::where([['event_id',$submission[0]->event_id],['team_id',$submission[0]->team_id]])->get();
    // dd($votings);
    if (isset($votings[0])) {
        # code...
    foreach ($votings as $key => $voting) {
        $member[] = $voting->user_id; 
    }
    // dd($member);
    $members = getUserDetails($member);
    }
    // dd($members);
    return view('front_view/voting-details',compact('submission','event','created_id','names','day','votings','members'));
  }

  public function rating(Request $request)
  {
    $rating = new Voting([
        'rating' => $request->get('rate'),
        'comment' => $request->get('comment'),
        'event_id' => $request->get('event_id'),
        'team_id' => $request->get('team_id'),
        'submission_id' => $request->get('sub_id'),
        'user_id' => session('user_id'),
        'created_by' => session('user_id'),
    ]); 
    $rating->save();
    return redirect::back();
  }

  public function check_email(Request $request,$email)
  {
     $user = User::where('email', $email)->get()->toArray();
     //dd($request->get("email"));
     // dd($user);
     if($user) 
     {
        return response()->json(['success' => false]);
     }
    else 
    {
        return response()->json(['success' => true]);
    }
}

  public function registration($id)
  {
    $id = decrypt_data($id);
    $event = Event::where([['is_spark' ,'=', "1"],['id',$id]])->get();

    $date =$event[0]->date;
    $temp = Carbon::parse($date); 
    $day = $temp->format('d M,Y - l');
    $min = $event[0]->min;
    $max = $event[0]->max;
    $loc = explode(',', $event[0]->location);
    $location = Location::whereIn('id',$loc)->get();
    $email = getUserDetails(session('user_id'));
    // dd($email);
    // $location = Location::all();
    // dd($location);
    return view('front_view/registration',compact('event','day','min','max','location','email'));
  }

  public function team_registration(Request $request)
  {
    // dd($request->get('participant_name'));
    $array = $request->get('participant_name');
    // dd($array);
    $checks = Team::whereHas('teamMembers', function($q) use($array) {
           
            $q->whereIn('email',$array);
            
        })->where('event_id',$request->get('event_id'))->get();
    // dd($checks);
    // dd($array);
    if (!isset($checks[0])) {
    $team = new Team([
        'team_name' => ucfirst($request->get('groupname')),
        'event_id' => $request->get('event_id'),
        'location_id' => $request->get('location'),
        'created_by' => session('user_id'),
    ]);
    $team->save();
    foreach ($request->get('participant_name') as $value) {
        // dd($value);
        $data = autosuggest_api($value);
        // dd($data['data']['result'][0]['email']);
        $id = $data['data']['result'][0]['id'];
        $email = $data['data']['result'][0]['email'];
       $members = new TeamMember([
        'team_id' => $team->id,
        'member_id' => $id,
        'email'=>$email,
        'created_by' => session('user_id'),
    ]);
       $members->save();
    }
    $id = encrypt_data($request->get('event_id'));
    // dd('/event/details/$id/coming');
    return redirect('/event/details/'.$id.'/coming');
    }
    else{
        return back()->with('InvalidMember',"Team Member Already Exist");
    }
  }
  public function checkEmail(Request $request){
        // dd($request->search);
        $result=array();
        $search=autosuggest_api($request->search);
        // dd($search);
        if($search['status']){
            $data=$search['data']['result'];
            foreach($data as $d){
                array_push($result,$d['email']);
            }
        }
        // dd($result);
        echo json_encode($result);
    }

  public function member(Request $request)
  {
    // dd($request->get('id'));
    $id = $request->get('id');
    $team_name = Team::find($id);
    // dd($team_name);
    $teams = TeamMember::where('team_id',$id)->get();

    foreach ($teams as $key => $team) {
        $member_id[] = $team->member_id; 
    }
    $user_id =session('user_id');
    if (in_array($user_id, $member_id)) {
        $check = true;
    }
    else{
        $check = false;
    }
    $name = getUserDetails($member_id);
    return response()->json(['success' => true,'name'=>$name,'id'=>$id,'team_name'=>$team_name,'check'=>$check]);
  }  

  public function submission($team,$event)
  {
    // $location = Location::all();
    // dd($team);
    $team = decrypt_data($team);
    $event = decrypt_data($event);
    $event_data = Event::where([['is_spark' ,'=', "1"],['id',$event]])->get();
    $date =$event_data[0]->date;
    $temp = Carbon::parse($date); 
    $day = $temp->format('d M,Y - l');
    $submission_data = Submission::with('subImages')->where('team_id',$team)->get();
    // dd($team[0]);
    return view('front_view/submission',compact('location','team','event','event_data','submission_data','day'));
  }

  public function edit_submission($id)
  {
    // $location = Location::all();
    // dd($id);
    $id = decrypt_data($id);
    $submission_data = Submission::with('subImages')->where('id',$id)->get();
    $event_data = Event::where([['is_spark' ,'=', "1"],['id',$submission_data[0]->event_id]])->get();
    $date =$event_data[0]->date;
    $temp = Carbon::parse($date); 
    $day = $temp->format('d M,Y - l');
    $name = getUserDetails($submission_data[0]->created_by);
    // dd($name[$submission_data[0]->created_by]['name']);
    // dd($submission_data);

    return view('front_view/edit_submission',compact('submission_data','name','event_data','day'));
  }

  public function team_submission(Request $request)
  {
    $sub_image = [];

    // $sub_data = Submission::where('team_id',$request->get('team_id'))->first();
    $location = Team::find($request->get('team_id'));
    // dd($sub_data);

    $submission = new Submission([
        'Title' => ucfirst($request->get('title')),
        'Description' => $request->get('description'),
        'event_id' => $request->get('event_id'),
        'team_id' => $request->get('team_id'),
        'location' => $location->location_id,
        'created_by' => session('user_id'),
    ]);
    $submission->save();
    foreach ($request['Image'] as $file ) {
        // dd($file);
        $path =public_path().'/events/'.'submission/'.$request->get('event_id').'/'.$request->get('team_id').'/';  
        // dd($path);
        $image = saveImage($file,$path);
        $sub_image[] = [
            'submission_id'=>$submission->id,
             'image'=>$image,
        ];
        
    }
    // dd($sub_image);
    $data = SubmissionImage::insert($sub_image);
    $sub_id = $submission->id;
    
    // return redirect()->route('voting-details', [$sub_id]);
    return redirect()->route('voting-details', [encrypt_data($sub_id)]);
  }

  public function del_team(Request $request)
  {
        // dd();
        $id =$request->get('team_id');
        $team = Team::find($id);
        $team->delete();
        return redirect::back(); 
  }

  public function update_submission(Request $request )
  {
        // dd($sub_data->id);
        // dd($request->get('id'));
        if ($request['Image']) {
            foreach ($request['Image'] as $file ) {
                $path =public_path().'/events/'.'submission/'.$request->get('event_id').'/'.$request->get('team_id').'/';
                // dd($path);
                $image = saveImage($file,$path);
                $sub_image[] = [
                    'submission_id'=>$request->get('id'),
                    'image'=>$image,
                ];
            }
            $data = SubmissionImage::insert($sub_image);
        }
        // $sub_id = $sub_data->id;
    return redirect('voting/'.encrypt_data($request->get('event_id')));
  }

  public function del_image(Request $request)
  {
    $id = $request->get('id');
    $data = SubmissionImage::with('submission')->where('id',$id)->first();
    // dd($data->image);
    $imagePath = public_path().'/events/submission/'.$data['submission']->event_id.'/'.$data['submission']->team_id.'/'.$data->image;
    File::delete($imagePath);
    $data->delete();
    return redirect::back();
  }

  // public function folder_upload() {

  //   $filesInFolder = File::files(public_path('events/2019/6/image/'));     
  //   foreach($filesInFolder as $path) { 
  //         $file = pathinfo($path);
  //         echo $file['filename'] ;
  //    } 
// } 
}

