<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Event;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    

// Event Master
public function event_list()
    {
        // Listing Event 
        return view('admin/event_list');
    }
    public function event_data()
    {
        // Databale for event 
        $Event = Event::all()->toArray();
        return Datatables::of($Event)
        ->addIndexColumn()
        ->addColumn('check', function($row){
         $btn = '<input type = "checkbox" value="'.$row["id"].'" class="delete" name="mult_delete[]">';
         return $btn;
        })
        ->addColumn('bliss', function($row){
                if ($row["is_bliss"]==1)
                 {
                    return 'Active';
                 }
                else
                {
                    return 'Inactive';
                }              
        })
        ->addColumn('spark', function($row){
                if ($row["is_spark"]==1)
                 {
                    return 'Active';
                 }
                else
                {
                    return 'Inactive';
                }              
        })
        ->addColumn('action', function($row){
            
                $btn = '<a style="margin-left: 4px;margin-bottom: 5px;padding: 7px 23px;" href="'.url("/edit_event/".$row["id"]).'" class="edit btn btn-primary btn-sm">EDIT</a>
                <input  type="hidden" name="del" value="DELETE"/>
                <button id="single_del" data-id="'.$row["id"].'" data-name="'.$row["title"].'" style="margin-left:5px;margin-top:-5px;" type="submit" class="btn btn-danger">DELETE</button>
                ';

                return $btn;
            

        })
        ->rawColumns(['action','check','bliss','spark'])
        ->make(true);
    }

    public function add_event_view() 
    {
      // Add event View
      return view('admin/add_event');

  }
public function add_event(Request $request)
{
      //Add event   
  // $validatedData = $request->validate(
  //   [
  //       'email' => 'required|email|unique:users,email',
  //   ]);
    //dd($request);

  session_start();
  $event = new Event([
    'title' => $request->get('title'),
    'description' => $request->get('description'),
    'date' => $request->get('date'),
    'is_bliss' => $request->get('bliss'),
    'is_spark' => $request->get('spark'),
    'created_by' => Auth::user()->id,
]);
  $event->save();
  $request->session()->flash('message.level', 'success');
  $request->session()->flash('message.content', 'Successfully Added');
  return redirect('event/list'); 
}
public function edit_event($id)
{
      // Update event view  
  $update = Event::find($id);

  return view('admin/edit_event',compact('update'));
}
public function update_event(Request $request,$id)
{
     //Update event function
 // $validatedData = $request->validate(
 //    [
 //        'email' => 'required|email|',
 //    ]);   
 session_start();                
 $event = Event::find($id);
 $event->title = $request->get('title');
 $event->description = $request->get('description');
 $event->date = $request->get('date');
 $event->is_bliss = $request->get('bliss');
 $event->is_spark = $request->get('spark');
 $event->updated_by = Auth::user()->id;
 $event->save();
 $request->session()->flash('message.level', 'success');
 $request->session()->flash('message.content', 'Successfully Updated');
 return redirect('event/list');
}
public function delete_event(Request $request)
{
      // Delete event   
  session_start();
  $id= $request->get('hidden_del'); 
  $event = Event::find($id);
  $event->delete();
  $request->session()->flash('message.level', 'success');
  $request->session()->flash('message.content', 'Successfully Deleted');
  return redirect('event/list');
}
public function delete_multi_event(Request $request)
{
      // Delete Multiple event
  session_start();
  $id=$request->post('id');
  foreach ($id as $key ) {
    $event = Event::find($key);
    $event->delete();
}  
$request->session()->flash('message.level', 'success');
$request->session()->flash('message.content', 'Successfully Deleted');
return response()->json(['success' => true]);
}
}
