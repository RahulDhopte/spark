<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserLoginController extends Controller
{
	 // public function __construct(Request $request)
  //   {
  //       $this->middleware('guest')->except('logout');
  //   }
    public function login(Request $request){

        // $request->validate(['email'=>'required',
        //                     'password'=>'required']);
    	// dd($request->email);
        $email=$request->email;
        $password=$request->password;
        $auth_result=authenticate_user($email,$password);
        // dd($auth_result);
        if($auth_result["is_valid"]==true){
            session(['user_is_valid'=>true]);
            session(['user_id'=>base64_decode($auth_result["id"])]);
            session(['user_name'=>$auth_result["name"]]);
            session(['user_type'=>$auth_result["user_type"]]);
            return redirect('/');
        }
        else
            return back()->with('invalid_user',"Invalid Email/Password");
       
    }
    

    public function logout(Request $request)
    {
        $request->session()->forget(['user_is_valid', 'user_id', 'user_name','user_type']);

        return redirect('/login');
    }
    protected function guard()
    {
        return Auth::guard();
    }
}
