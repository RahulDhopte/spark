<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    // public function __construct(Request $request)
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
        # code...
    protected $redirectTo = '/user/list';
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);


        $user=User::where('email',$request->email)->first();
        
        if($user->count()>0) {
            $auth_result=authenticate_user($request->email,$request->password);
            if($auth_result["is_valid"]==true){
                session(['mis_token'=>$auth_result['token']]);
                session(['admin_user_id'=>$user->id]);
                session(['admin_user_name'=>$user->name]);
                session(['admin_user_role'=>$user->role_id]);
                session(['admin_is_valid'=>true]);
                return redirect($this->redirectTo);
            }


        }
    }

    public function logout(Request $request)
    {
        
        $request->session()->forget(['admin_is_valid', 'admin_user_name', 'admin_user_id','admin_user_role','mis_token']);

        return redirect('/control/login');
    }
    
}
