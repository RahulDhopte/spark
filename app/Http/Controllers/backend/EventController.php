<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Event;
use App\EventImage;
use Image, File;
use Redirect;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    // 	$this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

// Event Master
    public function event_list()
    {
        // Listing Event 
    	return view('admin/event_list');
    }
    public function event_data()
    {
        // Databale for event 
    	$Event = Event::withCount(['eventApprovedImage', 'eventTotalImage'])->where('status' , '1')->get()->toArray();
       
        return Datatables::of($Event)
        ->addIndexColumn()
        ->addColumn('check', function($row){
            $btn = '<input type = "checkbox" value="'.$row["id"].'" class="delete" name="mult_delete[]">';
            return $btn;
        })
        ->addColumn('bliss', function($row){
            if ($row["is_bliss"]==1)
            {
                return '<i class="fa fa-check-circle status" data-value ="1" data-type ="bliss" data-id ='.$row["id"].' style="font-size:24px;color:green"></i>';
            }
            else
            {
                return '<i class="fa fa-times-circle status" data-value ="0" data-type ="bliss" data-id ='.$row["id"].'  style="font-size:24px;color:red"></i>';
            }              
        })
        ->addColumn('spark', function($row){
            if ($row["is_spark"]==1)
            {
                return '<i class="fa fa-check-circle status" data-value ="1" data-type ="spark"  data-id ='.$row["id"].'  style="font-size:24px;color:green"></i>';
            }
            else
            {
                return '<i class="fa fa-times-circle status" data-value ="0" data-type ="spark" data-id ='.$row["id"].'  style="font-size:24px;color:red"></i>';
            }              
        })
        ->addColumn('image', function($row){
            $btn = '<a href="'.url("/event/image/".$row["id"]).'" class="image ">Images ('.$row['event_approved_image_count'].'/'.$row['event_total_image_count'].')</a>';
            return $btn;             
        })
        ->addColumn('action', function($row){

            $btn = '<a style="padding: 7px 23px;" href="'.url("/edit_event/".$row["id"]).'" class="edit btn btn-primary btn-sm">EDIT</a>
            <input  type="hidden" name="del" value="DELETE"/>
            <button id="single_del" data-id="'.$row["id"].'" data-name="'.$row["title"].'" style="" type="submit" class="btn btn-danger">DELETE</button>
            ';

            return $btn;


        })
        ->rawColumns(['action','check','bliss','spark','image'])
        ->make(true);
        
    }

    public function add_event_view() 
    {
      // Add event View
    	return view('admin/add_event');

    }
    public function add_event(Request $request)
    {
      //Add event 
    	session_start();
    	$event = new Event([
    		'title' => $request->get('title'),
    		'description' => $request->get('description'),
    		'date' => date(config('app.date_format'), strtotime($request->get('date'))),
            'start_time' => $request->get('start_time'),
            'end_time' => $request->get('end_time'),
            'min' =>$request->get('min'),
            'max' =>$request->get('max'),
            'status' =>$request->get('status'),
    		'is_bliss' => $request->get('bliss'),
    		'is_spark' => $request->get('spark'),
    		'created_by' => Auth::user()->id,
    	]);
    	$event->save();
    	$request->session()->flash('message.level', 'success');
    	$request->session()->flash('message.content', 'Successfully Added');
    	return redirect('event/list'); 
    }
    public function edit_event($id)
    {
      // Update event view  
    	$update = Event::find($id);

    	return view('admin/edit_event',compact('update'));
    }
    public function update_event(Request $request,$id)
    {
     //Update event function
 // $validatedData = $request->validate(
 //    [
 //        'email' => 'required|email|',
 //    ]);   
    	session_start();                
    	$event = Event::find($id);
    	$event->title = $request->get('title');
    	$event->description = $request->get('description');
    	$event->date = date(config('app.date_format'), strtotime($request->get('date')));
        $event->start_time = $request->get('start_time');
        $event->end_time = $request->get('end_time');
        $event->min = $request->get('min');
        $event->max = $request->get('max');
    	$event->status = $request->get('status');
        $event->is_bliss = $request->get('bliss');
    	$event->is_spark = $request->get('spark');
    	$event->updated_by = Auth::user()->id;
    	$event->save();
    	$request->session()->flash('message.level', 'success');
    	$request->session()->flash('message.content', 'Successfully Updated');
    	return redirect('event/list');
    }
    public function delete_event(Request $request)
    {
      // Delete event   
    	session_start();
    	$id= $request->get('hidden_del'); 
    	$event = Event::find($id);
    	$event->delete();
    	$request->session()->flash('message.level', 'success');
    	$request->session()->flash('message.content', 'Successfully Deleted');
    	return redirect('event/list');
    }
    public function delete_multi_event(Request $request)
    {
      // Delete Multiple event
    	session_start();
    	$id=$request->post('id');
    	foreach ($id as $key ) {
    		$event = Event::find($key);
    		$event->delete();
    	}  
    	$request->session()->flash('message.level', 'success');
    	$request->session()->flash('message.content', 'Successfully Deleted');
    	return response()->json(['success' => true]);
    }
    public function event_image($id)
    {
    	$event = Event::find($id);
    	$year = now()->year;
    	$images = EventImage::where('event_id',$id)->get();
    	$path = '/events/'.$year.'/'.$event['id'].'/image';
    	return view('admin/event_images',compact('event','images','path'));
    }

    public function fileUpload($file, $event){
    	
    	foreach ($file as $key => $value) {
    		$year = now()->year;
    		$randonName = rand(1, 200);
    		$extension = File::extension($value->getClientOriginalName());
    		$name = File::name($value->getClientOriginalName());	

    		$img = Image::make($value->getRealPath());
    		$filename = time().'_'.$randonName.'.'.$extension;

    		
        //Upload File
    		
    			$thumbnail_path = public_path().'/events/'.$year.'/'.$event.'/'.'thumbnail';

    			if(!File::isDirectory($thumbnail_path)){
    				File::makeDirectory($thumbnail_path, 0777, true, true);
    			}

    			$thumbnail = Image::make($value->getRealPath());
    			$paths = public_path().'/events/'.$year.'/'.$event.'/thumbnail/'.$filename;
    			$this->createThumbnail($paths,$thumbnail, 150, 93);
    		
    		
    			$path = public_path().'/events/'.$year.'/'.$event.'/'.'image';
    			if(!File::isDirectory($path)){
    				File::makeDirectory($path, 0777, true, true);
    			}
    			$image = new EventImage([
    				'event_id' => $event,
    				'event_image' => time().'_'.$randonName.'.'.$extension,
    				'created_by' => Auth::user()->id,
    			]);
    			$image->save();
    			$img->save(public_path().'/events/'.$year.'/'.$event.'/image/'.$filename);
    		

    	}
    }

    public function upload_image(Request $request)
    {
    	if($request->hasFile('Image')) {
    		$year = now()->year;
    		$event = $request->get('event_id');
    		$randonName = rand(1, 200);

        //get filename with extension

    		$this->fileUpload($request->file('Image'),$event);
    	}
    	return redirect('/event/image/'.$event.'');
    }

    public function createThumbnail($path, $thumbnail, $width, $height)
    {
    	$thumbnail->resize($width, $height);
    	$thumbnail->save($path);
    }

    public function is_Approved(Request $request)
    {
    	$id = $request->post('id');
    	$img = EventImage::find($id);
    	$img->is_approved = '1';
    	$img->save();
    	$year = now()->year;
    	$event = $img['event_id'];
    	$path = '/events/'.$year.'/'.$event.'/image';
    	$name = $img['event_image'];
    	$img_id = $img['id'];
    	if ($img) {
    		return response()->json(['success' => true,'name' => $name,'img_id' => $img_id,'url' => url($path)]);
    	}
    	else{
    		return response()->json(['success' => false]);
    	}
    }
    public function is_UnApproved(Request $request)
    {
    	$id = $request->post('id');
    	$img = EventImage::find($id);
    	$img->is_approved = '0';
    	$img->save();
    	$year = now()->year;
    	$event = $img['event_id'];
    	$path = '/events/'.$year.'/'.$event.'/image';
    	$name = $img['event_image'];
    	$img_id = $img['id'];
    	if ($img) {
    		return response()->json(['success' => true,'name' => $name,'img_id' => $img_id,'url' => url($path)]);
    	}
    	else{
    		return response()->json(['success' => false]);
    	}
    }
    public function delete_event_image(Request $request)
    {
      // Delete event   
    	session_start();
    	$id= $request->get('hidden_del'); 
    	$year = now()->year;
    	$event = EventImage::find($id);
    	$name = $event['event_image'];
    	$event_id = $event['event_id'];
    	$thumbnailPath = public_path().'/events/'.$year.'/'.$event_id.'/thumbnail/'.$name;
    	$imagePath = public_path().'/events/'.$year.'/'.$event_id.'/image/'.$name;
    	// dd($imagePath);
 		File::delete($thumbnailPath);
 		File::delete($imagePath);
    	$event->delete();
    	$request->session()->flash('message.level', 'success');
    	$request->session()->flash('message.content', 'Successfully Deleted');
    	return redirect::back();
    }
    public function status(Request $request)
    {
    	$id = $request->get('hidden_id');
    	$type = $request->get('hidden_type');
    	$value = $request->get('hidden_value');
    	if ($type == 'bliss') {
    		if ($value == '0') {
		    	$event = Event::find($id);
		    	$event->is_bliss = '1';
		    	$event->save();
    		}
    		else{
    			$event = Event::find($id);
		    	$event->is_bliss = '0';
		    	$event->save();
    		}
		    	
    	}
    	else{
    		   if ($value == '0') {
		    	$event = Event::find($id);
		    	$event->is_spark = '1';
		    	$event->save();
    			}
    			else{
    			$event = Event::find($id);
		    	$event->is_spark = '0';
		    	$event->save();
    		}
    	}
    	return redirect('event/list');
    }
}

