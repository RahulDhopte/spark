<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Event;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class UserController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    // User Master
    public function user_list()
    {
        // Listing User 
        return view('admin/user_list');
    }
    public function user_data()
    {
        // Databale for User 
        $Users = User::with("role")->get()->toArray();
        return Datatables::of($Users)
        ->addIndexColumn()
        ->addColumn('check', function($row){
         $btn = '<input type = "checkbox" value="'.$row["id"].'" class="delete" name="mult_delete[]">';
         return $btn;
     })
        ->addColumn('role', function($row){
            return $row['role']['name']; 
        })
        ->addColumn('action', function($row){
            if (session('admin_user_id')!= $row["id"]) {
                $btn = '<a style="margin-left: 4px;margin-bottom: 5px;padding: 7px 23px;" href="'.url("/edit/".$row["id"]).'" class="edit btn btn-primary btn-sm">EDIT</a>
                <input  type="hidden" name="del" value="DELETE"/>
                <button id="single_del" data-id="'.$row["id"].'" data-name="'.$row["email"].'" style="margin-left:5px;margin-top:-5px;" type="submit" class="btn btn-danger">DELETE</button>
                ';

                return $btn;
            }

        })
        ->rawColumns(['action','role','check'])
        ->make(true);
    }

    public function add_user_view() 
    {
      // Add user View
      $role_name = Role::all();
      return view('admin/add_user',compact('role_name'));

  }
  public function check_email(Request $request,$email)
  {
     $user = User::where('email', $email)->get()->toArray();
     //dd($request->get("email"));
     // dd($user);
     if($user) 
     {
        return response()->json(['success' => false]);
     }
    else 
    {
        return response()->json(['success' => true]);
    }
}
public function add_user(Request $request)
{
      //Add User   
  $validatedData = $request->validate(
    [
        'email' => 'required|email|unique:users,email',
    ]);

  session_start();
  $user = new User([
    'email' => $request->get('email'),
    'role_id' => $request->get('role'),
    'password' =>Hash::make('password'),
]);
  $user->save();
  $request->session()->flash('message.level', 'success');
  $request->session()->flash('message.content', 'Successfully Added');
  return redirect('user/list'); 
}
public function edit_user($id)
{
      // Update User view  
  $update = User::find($id);
  $role_name = Role::all();

  return view('admin/edit_user',compact('update','role_name'));
}
public function update_user(Request $request,$id)
{
     //Update User function
 $validatedData = $request->validate(
    [
        'email' => 'required|email|',
    ]);   
 session_start();                
 $user = User::find($id);
 $user->email = $request->get('email');
 $user->role_id = $request->get('role');
 $user->save();
 $request->session()->flash('message.level', 'success');
 $request->session()->flash('message.content', 'Successfully Updated');
 return redirect('user/list');
}
public function delete_user(Request $request)
{
      // Delete User   
  session_start();
  $id= $request->get('hidden_del'); 
  $user = User::find($id);
  $user->delete();
  $request->session()->flash('message.level', 'success');
  $request->session()->flash('message.content', 'Successfully Deleted');
  return redirect('user/list');
}
public function delete_multi_user(Request $request)
{
      // Delete Multiple User
  session_start();
  $id=$request->post('id');
  foreach ($id as $key ) {
    $user = User::find($key);
    $user->delete();
}  
$request->session()->flash('message.level', 'success');
$request->session()->flash('message.content', 'Successfully Deleted');
return response()->json(['success' => true]);
}
public function admin_page()
{
    // Admin page 
    return view('admin/admin_page');
}
}
