<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voting extends Model
{
    protected $table = 'voting';
    protected $fillable = [
        'rating','comment','event_id','team_id','user_id','created_by','updated_by','submission_id',
    ];
}
