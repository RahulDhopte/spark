<?php 
use Illuminate\Support\Facades\Crypt;


if (!function_exists('encrypt_data')) {
    /**
     * Returns a human readable file size
     *
     * @param integer $data
     * Bytes contains the size of the bytes to convert
     *
     *
     *
     * @return string encrypted data
     *
     * */
    function encrypt_data($data)
    {
        
        return Crypt::encryptString($data);
        // return $data;
//        dd(base64_encode(encrypt($data)));
//        return base64_encode($data);

//        if($data==""){
           /* return "";
        }
        $result = base64_encode($encryption->encrypt($data));
        return $result;*/
 
    }
}

if (!function_exists('decrypt_data')) {
    /**
     * Returns a human readable file size
     *
     * @param integer $data
     * Bytes contains the size of the bytes to convert
     *
     * @param integer $decimals
     * Number of decimal places to be returned
     *
     * @return string a string in human readable format
     *
     * */
    function decrypt_data($data)
    {
        return Crypt::decryptString($data);
        // return $data;
//        return base64_decode($data);
        /*if($data==""){
            return "";
        }*/
//        $decryption = new App\Cencryption(config('app.wellness_enc_key'));
       /* $result = $encryption->decrypt(base64_decode($data));
        return $result;*/
 
    }
}

if(!function_exists('authenticate_user')){
    function authenticate_user($email,$password){
        $client = new lawiet\src\NuSoapClient("http://services.neosofttech.in/webservices/authentication.php");
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = FALSE;
        $error  = $client->getError();
        $data=[];
        $data['parameter']='authenticate_without_enc';
        $data['emailAddress']=$email;
        $data['password']=$password;
        // Calls
        $result = $client->call('Authenticate_Login', $data);

        if ($client->fault) {
            /*echo "<h2>Fault</h2><pre>";
            print_r($result);
            echo "</pre>";*/
            return $result;
        } else {
            $error = $client->getError();
            if ($error) {
                return $error;
//                echo "<h2>Error</h2><pre>" . $error . "</pre>";
            } else {
//                echo "<h2>Main</h2>";
                return $result;
            }
        }
    }
}
if(!function_exists('get_employees_list')){
    function get_employees_list($email,$token){
        $client = new lawiet\src\NuSoapClient("http://services.neosofttech.in/webservices/employeeslist.php");
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = FALSE;
        $error  = $client->getError();
        $data=[];
        $data['parameter']='emp_details';
        $data['emailAddress']=$email;
        $data['token']=$token;
        // Calls
        $result = $client->call('Return_Employee_List', $data);

        if ($client->fault) {
            /*echo "<h2>Fault</h2><pre>";
            print_r($result);
            echo "</pre>";*/
            return $result;
        } else {
            $error = $client->getError();
            if ($error) {
                return $error;
//                echo "<h2>Error</h2><pre>" . $error . "</pre>";
            } else {
//                echo "<h2>Main</h2>";
                return $result;
            }
        }
    }
}
if(!function_exists('getUserDetails')){
    function getUserDetails($data){

        if(gettype($data)=="string" || gettype($data)=="int"){
            $ids=$data;
        }else{
            $ids=implode(",",$data);
        }
        //dd($ids);
        $clientSoap = new lawiet\src\NuSoapClient("http://services.neosofttech.in/webservices/employee_details.php");
        $arrResultSoap = $clientSoap->call('Return_Employee_Details', array('parameter' => 'return_employee_name', 'id' => $ids));
        $user_data=[];
        if($arrResultSoap["is_valid"]==false){
            return $arrResultSoap;
        }
        foreach($arrResultSoap["result"] as $user){
            $user_data[]=$user;
        }
        // dd($user_data);
        return($user_data);

    }
}

function convertymdTomy($input){
    $dateobj=DateTime::createFromFormat('Y-m-d',$input);
    return $dateobj->format('M-Y');
}

function getTotalDays($current_month){
    return date("t",strtotime($current_month));
}
function autosuggest_api($email='')
{
    $result = ['status' => false, 'data' => ''];

    $client = new lawiet\src\NuSoapClient("http://services.neosofttech.in/webservices/employee_details.php");
    $error = $client->getError();
    if ($error) {
        $error = "Constructor error: " . $error;
        \Illuminate\Support\Facades\Session::flash('error',"Constructor error: ".$error);

    } else {
        $arrResult = $client->call('Return_Employee_Details', array('parameter' => 'events_employees_listing', 'employee' => $email));
        $result = ['status' => true, 'data' => $arrResult];
    }
    return $result;
}

function getUserDetails($data){
        //dept id 3 wont receive emails
        // dd(gettype($data));
        if(gettype($data)=="string" || gettype($data)=="int" || gettype($data)=="integer"){
            $ids=$data;
        }else{
            $ids=implode(",",$data);
        }
        //dd($ids);
        $clientSoap = new lawiet\src\NuSoapClient("http://services.neosofttech.in/webservices/employee_details.php");
        $arrResultSoap = $clientSoap->call('Return_Employee_Details', array('parameter' => 'return_employee_name_with_mobile', 'id' => $ids));
        /*echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($clientSoap->request, ENT_QUOTES) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($clientSoap->response, ENT_QUOTES) . "</pre>";
exit;*/
        $user_data=[];
        if($arrResultSoap["is_valid"]==false){
            return $arrResultSoap;
        }
//        dd($arrResultSoap);
        foreach($arrResultSoap["result"] as $user){
            $user_data[$user["id"]]=$user;
        }

        return($user_data);

    }

    function saveImage($file,$path){
    $randonName = rand(1, 200);
      $extension = File::extension($file->getClientOriginalName());   

      $img = Image::make($file->getRealPath());
      $filename = time().'_'.$randonName.'.'.$extension;

      // $path = public_path().'/events/'.'sparkicon';
      if(!File::isDirectory($path)){
        File::makeDirectory($path, 0777, true, true);
      }
      if(!File::isDirectory($path.'medium_img')){
        File::makeDirectory($path.'medium_img', 0777, true, true);
      }
      $medium_img = Image::make($file)->resize(360,200);
      $medium_img->save($path.'medium_img/'.$filename);
            // dd(public_path().'/events/'.$year.'/'.$id.'/image/'.$filename);
      $img->save($path.$filename);
      return($filename);
}


