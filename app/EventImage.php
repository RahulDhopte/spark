<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventImage extends Model
{
     protected $table = 'event_images';
    protected $fillable = [
        'event_id','event_image','date','is_approved','created_by','updated_by',
    ];
}
