<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
     protected $table = 'teamMembers';
    protected $fillable = [
        'team_id','member_id','email','created_by','updated_by',
    ];
    public function team(){
    	return $this->belongsTo('App\Team', 'team_id', 'id');
    }
}
