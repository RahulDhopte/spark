<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submission_images', function (Blueprint $table) {
            $table->integer('submission_id')->unsigned()->index()->change();
            $table->foreign('submission_id')->references('id')->on('submission')->onDelete('cascade');
        });

        Schema::table('submission', function (Blueprint $table) {
            $table->integer('team_id')->unsigned()->index()->change();
            $table->foreign('team_id')->references('id')->on('team')->onDelete('cascade');
            $table->integer('event_id')->unsigned()->index()->change();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade')    ;
        });

        Schema::table('voting', function (Blueprint $table) {
            $table->integer('team_id')->unsigned()->index()->change();
            $table->foreign('team_id')->references('id')->on('team')->onDelete('cascade');
            $table->integer('event_id')->unsigned()->index()->change();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('submission_images', function (Blueprint $table) {
            $table->dropForeign('submission_id');
            $table->dropIndex('submission_id');
            // $table->dropColumn('submission_id');
        });

        Schema::table('submission', function (Blueprint $table) {
            $table->dropForeign('team_id');
            $table->dropIndex('team_id');
            // $table->dropColumn('team_id');

            $table->dropForeign('event_id');
            $table->dropIndex('event_id');
        });

        Schema::table('voting', function (Blueprint $table) {
            $table->dropForeign('event_id');
            $table->dropIndex('event_id');
            // $table->dropColumn('event_id');

            $table->dropForeign('team_id');
            $table->dropIndex('team_id');
        });
    }
}
