<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTeamId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teamMembers', function (Blueprint $table) {
            $table->integer('team_id')->unsigned()->index()->change();
            $table->foreign('team_id')->references('id')->on('team')->onDelete('cascade')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teamMembers', function (Blueprint $table) {
            //
        });
    }
}
