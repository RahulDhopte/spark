<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubIdToVoting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voting', function (Blueprint $table) {
            $table->integer('submission_id')->unsigned()->index()->after('team_id');
            $table->foreign('submission_id')->references('id')->on('submission')->onDelete('cascade')->change();
            // $table->integer('submission_id')->after('team_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voting', function (Blueprint $table) {
            $table->dropForeign('submission_id');
            $table->dropIndex('submission_id');
        });
    }
}
