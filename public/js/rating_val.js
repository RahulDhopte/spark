$(document).ready(function () {

	$("#rating_validate").validate({
		rules:
		{
			rate:
			{
				required: true
			},
			
		},
		message:
		{
					
		},
		errorPlacement: function(error, element) {
           if (element.attr("type") == 'radio') {
                 error.appendTo(element.parent());
            }
              else {
                error.insertAfter(element);

            }
        },
	});
});