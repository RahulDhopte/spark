$(document).ready(function () {

	$.validator.addMethod("emailchk", function(value,element) {
		if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(value)){
			return true;
		}
		else{
			return false;
		};
	},"Enter proper email");

	$("#user_validate").validate({
		rules:
		{
			email:
			{
				emailchk: true,
				required: true,
			},
			
		},
		message:
		{
					
		},
	});
});