$(document).ready(function () {

	$.validator.addMethod("noSpace", function(value, element) { 
		if ( value.trim().length != 0){ 
			return true ;
		} 
		else{
			return false;
		};
	},"Invalid");

	

	$("#sub").validate({
		rules:
		{
			title:
			{
				noSpace: true,
				required: true,
			},
			Description:
			{
				noSpace: true,
				required: true,
			},
			'Image[]': {
            	required: true,
            	extension: "jpg,jpeg,png"
        	},
			
		},
		message:
		{
						
		},
		errorPlacement: function(error, element) {
           if (element.attr("type") == 'radio') {
                 error.appendTo(element.parent());
            }
              else {
              	console.log(element);
              	 // error.appendTo(element);
                error.insertAfter(element);

            }
        },
	});

	$("#sub_img").validate({
		rules:
		{
			'Image[]': {
            	extension: "jpg,jpeg,png"
        	},
			
		},
		message:
		{
						
		},
		errorPlacement: function(error, element) {
           if (element.attr("type") == 'radio') {
                 error.appendTo(element.parent());
            }
              else {
              	console.log(element);
              	 // error.appendTo(element);
                error.insertAfter(element);

            }
        },
	});
});