$(document).ready(function () {

	$.validator.addMethod("noSpace", function(value, element) { 
		if ( value.trim().length != 0){ 
			return true ;
		} 
		else{
			return false;
		};
	},"Invalid");

	

	$("#registration_validate").validate({
		rules:
		{
			groupname:
			{
				noSpace: true,
				required: true,
			},
			"participant_name[0]":
			{
				noSpace: true,
				required: true,
			},
			// location:
			// {
			// 	required: true
			// }
			
		},
		message:
		{
						
		},
		errorPlacement: function(error, element) {
           if (element.attr("type") == 'radio') {
                 error.appendTo(element.parent());
            }
              else {
              	console.log(element);
              	 // error.appendTo(element);
                error.insertAfter(element);

            }
        },
	});
});