$(document).ready(function () {

	$.validator.addMethod("noSpace", function(value, element) { 
		if ( value.trim().length != 0){ 
			return true ;
		} 
		else{
			return false;
		};
	},"Invalid");

	$("#event_validate").validate({
		rules:
		{
			title:
			{
				noSpace: true,
				required: true,
			},
			description:
			{
				noSpace: true,
				required: true,
			},
			date:
			{
				noSpace: true,
				required: true,
			},
			status:
			{
				required: true
			},
			bliss:
			{
				required: true
			},
			spark:
			{
				required: true
			}
			
		},
		message:
		{
					
		},
		errorPlacement: function(error, element) {
           if (element.attr("type") == 'radio') {
                 error.appendTo(element.parent());
            }
              else {
                error.insertAfter(element);

            }
        },
	});
});