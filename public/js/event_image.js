$(document).ready(function () {

	 $("#image_form").validate({
	 	 rules: {
        'Image[]': {
            required: true,
            extension: "jpg,jpeg,png"
        }
    },
    messages: {
        'Image[]': {
            required: "Please upload file.",
            extension: "Please upload file in these format only (jpg, jpeg, png, ico, bmp)."
        }
    },
    })
});