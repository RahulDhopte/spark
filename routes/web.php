<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['Usercheck']], function () {
Route::get('/login', function () {
    return view("auth/login");
});
});

Route::post('/admin/login', 'Auth\LoginController@login')->name('login');
Route::post('/admin/logout', 'Auth\LoginController@logout')->name('logout');

// Route::group(['middleware' => ['loginAdmin']], function () {
// Route::group(['prefix' => 'control'], function(){
// Route::get('/', function () {
//     return redirect("/control/login");
// });

// Route::get('/login', function () {
//     return view("admin/admin_login");
// });
// });
// });

//Frontend routes 
Route::post('/login/front', 'Auth\UserLoginController@login')->name('login_front');
Route::post('/user/logout', 'Auth\UserLoginController@logout')->name('user_logout');
// Route::get('/admin', 'UserController@admin_page');

//Frontend routes  ->middleware('Voting')
Route::group(['middleware' => ['loginUser']], function () {
Route::get('/', 'Front_end\FrontController@index')->middleware('Event_check');
Route::get('/gallery', 'Front_end\FrontController@gallery');
Route::get('/upcoming/events', 'Front_end\FrontController@upcoming');
Route::get('/past/events', 'Front_end\FrontController@past');
Route::get('/event/details/{id}/{year}', 'Front_end\FrontController@event_details');
Route::get('/past/event/details/{id}', 'Front_end\FrontController@past_event_details');
Route::post('/event', 'Front_end\FrontController@event');
Route::post('/load/img', 'Front_end\FrontController@load_img');
Route::get('/folder_upload', 'Front_end\FrontController@folder_upload');
Route::get('/submission/{team}/{event}', 'Front_end\FrontController@submission')->middleware('Teamcheck');
Route::get('/edit/submission/{id}', 'Front_end\FrontController@edit_submission');
Route::post('/team/submission', 'Front_end\FrontController@team_submission')->name('team-submission');
Route::post('/update/submission', 'Front_end\FrontController@update_submission')->name('edit-submission');
Route::get('/voting/{id}', 'Front_end\FrontController@voting');
Route::get('/voting/details/{id}', 'Front_end\FrontController@voting_details')->name('voting-details');
Route::get('/registration/{id}', 'Front_end\FrontController@registration');
Route::get('/team/registration', 'Front_end\FrontController@team_registration');
Route::get('/get/member', 'Front_end\FrontController@member');
Route::get('/checkemail', 'Front_end\FrontController@checkEmail')->name('check-email');
Route::post('/del/image', 'Front_end\FrontController@del_image')->name('delete-image');
Route::post('/del/team', 'Front_end\FrontController@del_team')->name('del-team');
Route::post('/rating', 'Front_end\FrontController@rating')->name('rating');
Route::get('/check_email/{email}', 'Front_end\FrontController@@check_email');
});

// Auth::routes();
// Route::group(['middleware' => ['checkRole']], function () {
// //user routes
// Route::get('/user/list', 'backend\UserController@user_list');
// Route::get('/user_data', 'backend\UserController@user_data');
// Route::get('/edit/{id}', 'backend\UserController@edit_user');
// Route::get('/add/user/view', 'backend\UserController@add_user_view');
// Route::post('/update/user/{id}', 'backend\UserController@update_user');
// Route::post('/add/user', 'backend\UserController@add_user');
// Route::get('/check_email/{email}', 'backend\UserController@check_email');
// Route::post('/delete/user', 'backend\UserController@delete_user');
// Route::post('/delete/multiple/users', 'backend\UserController@delete_multi_user');
// //event routes
// Route::get('/event/list', 'backend\EventController@event_list');
// Route::get('/event_data', 'backend\EventController@event_data');
// Route::get('/edit_event/{id}', 'backend\EventController@edit_event');
// Route::get('/add/event/view', 'backend\EventController@add_event_view');
// Route::post('/update/event/{id}', 'backend\EventController@update_event');
// Route::post('/add/event', 'backend\EventController@add_event');
// Route::post('/delete/event', 'backend\EventController@delete_event');
// Route::post('/delete/multiple/event', 'backend\EventController@delete_multi_event');
// Route::get('/event/image/{id}', 'backend\EventController@event_image');
// Route::post('/image-upload', 'backend\EventController@upload_image');
// Route::post('/is_Approved', 'backend\EventController@is_Approved');
// Route::post('/is_UnApproved', 'backend\EventController@is_UnApproved');
// Route::post('/status', 'backend\EventController@status');
// Route::post('/delete/event/image', 'backend\EventController@delete_event_image');
// });
